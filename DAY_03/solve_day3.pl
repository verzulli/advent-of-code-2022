#!/usr/bin/perl

use strict;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------
  
# We'll use an utility function
sub getScore($);

# Let's open the file for reading
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Let's initialize the temporary score SUM
my $totalScore = 0;

# Let's read the file, line by line
while (my $line = <FH>) {

  # let's get rid of the terminating newline
  chomp($line);

  # let's split the line in two halves
  # ...we should check for odd-lengths.. but let's disgress, for the moment...
  my $tLen = length($line) / 2;
  my $p1 = substr $line, 0, $tLen;
  my $p2 = substr $line, $tLen;

  my $score = 0;
  # Let's loop over first half, one char at a time...
  foreach my $idx (1..$tLen) {

    # extract the $idx-th char
    my $c = substr $p1, $idx - 1, 1;

    # Let's process it, but ONLY if we have not processed it beforehand
    # (each char is counted only once, regardless of the duplications)
    if (index((substr $p1, 0, $idx - 1), $c ) == -1) {
      if (index($p2, $c) >= 0) {
        $score = getScore($c);

        # update the total score
        $totalScore = $totalScore + $score;
      }
    } else {
      # printf("[%s] already processed!\n", $c);
    }
  }
  printf("[%s] => [%d] temp total:[%d]\n", $line, $score, $totalScore);
}
print "Total score: [" . $totalScore . "]\n";
exit;

#
# receive a single char and return its "value"
# Calculation is based on the ASCII value
sub getScore($) {
  # ASCII CODES:
  #   a => 97 (subtract 96)
  #   z => 122 (subtract 96)
  #   A => 65 (subtract 38)
  #   Z => 90 (subtract 38)
  my ($char) = @_;

  my $val = 0;
  if ($char =~ /[a-z]/) {
    $val = +(ord($char)) - 96;
  } elsif ($char =~ /[A-Z]/) {
    $val = +(ord($char)) - 38;
  } else {
    $val = 0;
  }
  return $val;
}
