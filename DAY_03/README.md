TL/DR;
======
Being given a list of STRINGs (made by a-z|A-Z; case-sensitive), we need to split them in two halves and search for intersections between the two halves. When found, chars in the intersections need to be evaluated based on their ASCII code value (roughly...). We need to calculate the sum of such calculation

Original text
=============

    [...]
    The list of items for each rucksack is given as characters all on a single line. A given rucksack always has the same number of items in each of its two compartments, so the first half of the characters represent items in the first compartment, while the second half of the characters represent items in the second compartment.

    For example, suppose you have the following list of contents from six rucksacks:

    vJrwpWtwJgWrhcsFMMfFFhFp
    jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
    PmmdzqPrVvPwwTWBwg
    wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
    ttgJtRGJQctTZtZT
    CrZsJsPPZsGzwwsLwLmpwMDw

        The first rucksack contains the items vJrwpWtwJgWrhcsFMMfFFhFp, which means its first compartment contains the items vJrwpWtwJgWr, while the second compartment contains the items hcsFMMfFFhFp. The only item type that appears in both compartments is lowercase p.
        The second rucksack's compartments contain jqHRNqRjqzjGDLGL and rsFMfFZSrLrFZsSL. The only item type that appears in both compartments is uppercase L.
        The third rucksack's compartments contain PmmdzqPrV and vPwwTWBwg; the only common item type is uppercase P.
        The fourth rucksack's compartments only share item type v.
        The fifth rucksack's compartments only share item type t.
        The sixth rucksack's compartments only share item type s.

    To help prioritize item rearrangement, every item type can be converted to a priority:

        Lowercase item types a through z have priorities 1 through 26.
        Uppercase item types A through Z have priorities 27 through 52.

    In the above example, the priority of the item type that appears in both compartments of each rucksack is 16 (p), 38 (L), 42 (P), 22 (v), 20 (t), and 19 (s); the sum of these is 157.

    Find the item type that appears in both compartments of each rucksack. What is the sum of the priorities of those item types?

My notes
========
I'm going to implement the solution in [PERL](https://www.perl.org), as it's perfect to read input from files and to work properly with strings.

* My input data are in the "input.txt"
* My implemented solution is "solve_day3.pl"

Solving approach
----------------

* read the line
* split it in two (`$p1` and `$p2`)
* loop over `$p1`. one char at a time
* if char `$c` has not been already processed, process it...
* ...by checking if it's present in `$p2` and, if yes, calculating the score
* keep track of total score

Implementation
==============

Please refer to:
* [`solve_day3.pl`](./solve_day3.pl) - for Part 1
* [`solve_day3_P2.pl`](./solve_day3_P2.pl) - for Part 2

Results
=======

    [verzulli@XPSGarr DAY_03]$ perl  solve_day3.pl  | head  -n 10
    [PnJJfVPBcfVnnPnBFFcggttrtgCrjDtSjzSS] => [3] temp total:[3]
    [llWlLbmmdLLwLbqGdmTmbZfCQtzrMQfrjSzrtSrMqgMt] => [17] temp total:[20]
    [sHlZTsWZwGGlZmGTmdlZbhJNRPphVfRvVnRBsRsJJV] => [19] temp total:[39]
    [fsHtVbjtqstBghhwwPBw] => [20] temp total:[59]
    [SDQzzSzQrQMmmQlmlcNcJLZPgLrVZTdCddhgdPwwCw] => [18] temp total:[77]
    [JmSWSVGGlJJbRsbpWHfbRj] => [49] temp total:[126]
    [tJndRtwtddPvllvfrldrfPpHWDgglFDWWmMmHWmHpZlS] => [12] temp total:[138]
    [BBJTTjCsJWZCmSHSZD] => [29] temp total:[167]
    [LhqLcVzshTNjhqhcjLLTLjbTnGndfdwrfPRVRrdnwftQwJRv] => [48] temp total:[215]
    [wHlPJZwbbZfqbFwqFZfrrcrJrtMWSMMVtVcJht] => [36] temp total:[251]
    [...]
    [NZBBnGJNnNPWTcTTmVhZCh] => [52] temp total:[7745]
    [PSzgSgwrnzrSzBGJSJrSLQqfMHQfqgRgfjHLljll] => [7] temp total:[7752]
    [RgbNmBbqgWHWRNRqHtcMlMwJJjcDtVlD] => [34] temp total:[7786]
    [SzpFLGPddSGnnSLQZLtJJcclDlVjDQwMhDcc] => [43] temp total:[7829]
    [LtTZCTttRqqqvqTN] => [46] temp total:[7875]
    Total score: [7875]

Results for P2
==============

    [verzulli@XPSGarr DAY_03]$ perl solve_day3_P2.pl 
    Block:
    [PnJJfVPBcfVnnPnBFFcggttrtgCrjDtSjzSS]
    [llWlLbmmdLLwLbqGdmTmbZfCQtzrMQfrjSzrtSrMqgMt]
    [sHlZTsWZwGGlZmGTmdlZbhJNRPphVfRvVnRBsRsJJV]
        [f] => [6][6]

    Block:
    [fsHtVbjtqstBghhwwPBw]
    [SDQzzSzQrQMmmQlmlcNcJLZPgLrVZTdCddhgdPwwCw]
    [JmSWSVGGlJJbRsbpWHfbRj]
        [V] => [48][54]

    Block:
    [tJndRtwtddPvllvfrldrfPpHWDgglFDWWmMmHWmHpZlS]
    [BBJTTjCsJWZCmSHSZD]
    [LhqLcVzshTNjhqhcjLLTLjbTnGndfdwrfPRVRrdnwftQwJRv]
        [J] => [36][90]
    [...]
    Block:
    [nJLgNcQDNMlQHMvCbv]
    [zphFpmTszmwhGGFhhtppNfffVlvZvHCCVZzbfzvS]
    [mTTsmTRGstsFhWwtWjPRdjnJdjJnLjcLNd]
        [N] => [40][2431]

    Block:
    [MfBDjllflHLTpDhhppDDbp]
    [NZBBnGJNnNPWTcTTmVhZCh]
    [PSzgSgwrnzrSzBGJSJrSLQqfMHQfqgRgfjHLljll]
        [B] => [28][2459]

    Block:
    [RgbNmBbqgWHWRNRqHtcMlMwJJjcDtVlD]
    [SzpFLGPddSGnnSLQZLtJJcclDlVjDQwMhDcc]
    [LtTZCTttRqqqvqTN]
        [t] => [20][2479]

    Total score: [2479]