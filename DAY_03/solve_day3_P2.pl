#!/usr/bin/perl

use strict;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------
  
# We'll use an utility function
sub getScore($);

# Let's open the file for reading
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Let's initialize the temporary score SUM
my $totalScore = 0;

# Let's read the file, line + line + line...
while (my $line_1 = <FH>) {
  
  # let's get rid of the terminating newline
  chomp($line_1);

  # Read additional second and third line
  my $line_2 = <FH>; chomp $line_2;
  my $line_3 = <FH>; chomp $line_3;

  # Let's calculate the LENGTH of first line
  my $tLen = length($line_1);

  my $score = 0;
  my $found = 0;

  # Let's loop over first line, to search matches in second and third...
  foreach my $idx (1..$tLen) {

    # extract the $idx-th char
    my $c = substr $line_1, $idx - 1, 1;

    # move to next char, if it's not in $line_2
    next if (index($line_2, $c) == -1);

    # move to next char, if it's not in $line_3
    next if (index($line_3, $c) == -1);

    # if we're here, a match has been found. 
    # Let's calculate its score
    $score = getScore($c);

    # update the total score
    $totalScore = $totalScore + $score;

    printf("Block:\n [%s]\n [%s]\n [%s]\n\t[%s] => [%d][%d]\n\n", $line_1, $line_2, $line_3, $c, $score, $totalScore);

    # now we can stop here... and move to next three-line block
    last;
  }
}
print "Total score: [" . $totalScore . "]\n";
exit;

#
# receive a single char and return its "value"
# Calculation is based on the ASCII value
sub getScore($) {
  # ASCII CODES:
  #   a => 97 (subtract 96)
  #   z => 122 (subtract 96)
  #   A => 65 (subtract 38)
  #   Z => 90 (subtract 38)
  my ($char) = @_;

  my $val = 0;
  if ($char =~ /[a-z]/) {
    $val = +(ord($char)) - 96;
  } elsif ($char =~ /[A-Z]/) {
    $val = +(ord($char)) - 38;
  } else {
    $val = 0;
  }
  return $val;
}
