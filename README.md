What's this?
============

This project contains my notes and solutions for the [Advent of Code 2022](https://adventofcode.com/2022/about) challenges.

As you probably know, they are 25 programming challenges that you should solve based on your programming skills...

I just published this very project to keep track of my progress

Each single challenge is summarized and implemented in related folder.

--------

> As you will see, I stopped at DAY_13. At that stage, the time require 
> (for me) to solve that challenges started getting really significant and...
> as Christimas were quickly approaching (BTW: I started much later on december)
> I opted to stop there... leaving this very project to interested people
>
> It has been a very interesting experience and... you can be sure that on 
> Dec 1st, 2023, I'll be there, waiting to re-start :-)

--------

* [DAY 1: Calorie Counting - **solved!**](DAY_01/README.md)
* [DAY 2: Rock Paper Scissors - **solved!**](DAY_02/README.md)
* [Day 3: Rucksack Reorganization - **solved!**](DAY_03/README.md)
* [Day 4: Camp Cleanup - **solved!**](DAY_04/README.md)
* [Day 5: Supply Stacks - **solved!**](DAY_05/README.md)
* [Day 6: Tuning Trouble - **solved!**](DAY_06/README.md)
* [Day 7: No Space Left On Device - **solved!**](DAY_07/README.md)
* [Day 8: Treetop Tree House - **solved!**](DAY_08/README.md)
* [Day 9: Rope Bridge - **solved!**](DAY_09/README.md)
* [Day 10: Cathode-Ray Tube - **solved!**](DAY_10/README.md)
* [Day 11: Monkey in the Middle - **solved**](DAY_11/README.md)
* [Day 12: Hill Climbing Algorithm - **solved**](DAY_12/README.md)
* [Day 13: Distress Signal - **only P1 solved**](DAY_13/README.md)
* DAY 14: ~~*to be done*~~ *NOT implemented*
* DAY 15: ~~*to be done*~~ *NOT implemented*
* DAY 16: ~~*to be done*~~ *NOT implemented*
* DAY 17: ~~*to be done*~~ *NOT implemented*
* DAY 18: ~~*to be done*~~ *NOT implemented*
* DAY 19: ~~*to be done*~~ *NOT implemented*
* DAY 20: ~~*to be done*~~ *NOT implemented*
* DAY 21: ~~*to be done*~~ *NOT implemented*
* DAY 22: ~~*to be done*~~ *NOT implemented*
* DAY 23: ~~*to be done*~~ *NOT implemented*
* DAY 24: ~~*to be done*~~ *NOT implemented*
* DAY 25: ~~*to be done*~~ *NOT implemented*

Why this project?
=================

Because I like spending time programming and... have some free time to spend in front of my PC.

...and also, 'cause I really like to "share" my outcomes with people that, maybe, could find it useful! :-)