TL/DR;
======
Being given a path made by steps specified in a long list of text, you need to move an HEAD and a TAIL along such a path following certain rule.
As for P1, You need to count the number of places visited by TAIL

Original text
=============

    [...]
    in fact, the head (H) and tail (T) must always be touching (diagonally adjacent and even overlapping both count as touching):
    [...]
    If the head is ever two steps directly up, down, left, or right from the tail, the tail must also move one step in that direction so it remains close enough:
    [...]
    Otherwise, if the head and tail aren't touching and aren't in the same row or column, the tail always moves one step diagonally to keep up:
    [...]

My notes
========
I'm going to implement the solution in [PERL](https://www.perl.org).

* My input data are in the "input.txt"
* My implemented solutions are `solve_day9.pl` for P1 and`solve_day9_P2.pl` for P2

Solving approach
----------------
There is no need to keep the whole map of the path. I simply opted to track HEAD position (X;Y) and related TAIL position (X;Y).

When moving TAIL, I keep track of visited places with an HASH, so easy the process of counting distinct places (aka: the solution of P1)

As for P2, I changed a little the architecture, by introducing 2 x 10-element ARRAY  (one for Xs, the other for Ys) where I keep track of related item position. 
Of course, I needed to update the `UpdatePositions()` routine, so to loop over the whole array.

Implementation
==============

Please:
* refer to [`solve_day9.pl`](./solve_day9.pl) and [`solve_day9_P2.pl`](./solve_day9_P2.pl)

Results
=======

    [verzulli@XPSGarr DAY_09]$ perl solve_day9.pl
    Cur Status => H:[  0;  1] T:[  0;  0]
    Cur Status => H:[  0;  2] T:[  0;  1]
    Cur Status => H:[  1;  2] T:[  0;  1]
    Cur Status => H:[  1;  1] T:[  0;  1]
    Cur Status => H:[  0;  1] T:[  0;  1]
    Cur Status => H:[  0;  2] T:[  0;  1]
    Cur Status => H:[  0;  3] T:[  0;  2]
    Cur Status => H:[ -1;  3] T:[  0;  2]
    [...]
    -31;-44: [13]
    -10;10: [13]
    -1;3: [14]
    -4;-3: [16]
    -9;8: [16]
    Tot visited by tail: [5878]

Results for P2
==============

    [verzulli@XPSGarr DAY_09]$ perl solve_day9_P2.pl
    -183;-280: [1]
    -92;-260: [9]
    -141;-310: [9]
    -121;8: [9]
    -51;-135: [9]
    -63;35: [9]
    -111;-279: [9]
    -42;-56: [9]
    -134;-22: [9]
    -49;-191: [9]
    [...]
    -6;-8: [621]
    -5;5: [648]
    -4;4: [702]
    -6;6: [720]
    -103;31: [729]
    -59;56: [747]
    -32;22: [864]
    -115;44: [900]
    -7;1: [963]
    Tot visited by tail: [2405]
