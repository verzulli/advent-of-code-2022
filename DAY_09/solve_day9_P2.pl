#!/usr/bin/perl

use strict;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

# to update visited locations
sub UpdateVisited($$);
sub UpdatePositions();
sub DumpPosition();

# Initial X Position (from 0 [head] to 9 [tail])
my @X_pos = (0,0,0,0,0,0,0,0,0,0);
my @Y_pos = (0,0,0,0,0,0,0,0,0,0);

# to hold teh visited coords
my %visited;

# Let's open the file for reading...
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");
while (my $line = <FH>) {

  # remove ending newline
  chomp($line);

  # Do we have a proper line (one char and one num)?
  if ($line =~ /^(\w)\s(\d+)$/) {
    # yes! Extract move and steps
    my ($move, $step) = ($1, $2);

    if ($move eq 'U') {
      foreach my $s (1..$step) {
        $Y_pos[0]--;
        UpdatePositions();
      }
    } elsif ($move eq 'D') {
      foreach my $s (1..$step) {
        $Y_pos[0]++;
        UpdatePositions();
      }
    } elsif ($move eq 'L') {
      foreach my $s (1..$step) {
        $X_pos[0]--;
        UpdatePositions();
      }
    } elsif ($move eq 'R') {
      foreach my $s (1..$step) {
        $X_pos[0]++;
        UpdatePositions();
      }
    } else {
      print "Bad move! $line\n";
      exit;
    }
  } else {
    print "Bad move! $line\n";
    exit;
  }

}

# print the score table, sorted by value
foreach my $k (sort {$visited{$a} <=> $visited{$b}} keys %visited) {
  printf("%s: [%d]\n", $k, $visited{$k});
}
print "Tot visited by tail: [" . scalar(keys %visited) . "]\n";
exit;

sub UpdateVisited($$) {
  my ($x, $y) = @_;

  my $pos_id = sprintf("%d;%d",  $x, $y);
  $visited{$pos_id}++;
}

sub UpdatePositions() {
  # If the head is ever two steps directly up, down, left, or right 
  # from the tail, the tail must also move one step in that direction 
  # so it remains close enough:

  # print "----- NUOVA MOSSA--------\n";

  for my $idx (1..9) {

    # Same Y / two step X
    if ($Y_pos[$idx] == $Y_pos[$idx-1]) {
      if ($X_pos[$idx] - $X_pos[$idx-1] == 2) {
        $X_pos[$idx]--;
      } elsif ($X_pos[$idx] - $X_pos[$idx-1] == -2) {
        $X_pos[$idx]++;
      } elsif (abs($X_pos[$idx] - $X_pos[$idx-1]) <= 1) {
        # do nothing
      } else {
        print "This should never happen - A!";
        printf("Cur Status => H:[%3d;%3d] T:[%3d;%3d]\n",$X_pos[$idx-1], $Y_pos[$idx-1], $X_pos[$idx], $Y_pos[$idx]);
        exit;
      }
    };

    # Same X / two step Y
    if ($X_pos[$idx] == $X_pos[$idx-1]) {
      if ($Y_pos[$idx] - $Y_pos[$idx-1] == 2) {
        $Y_pos[$idx]--;
      } elsif ($Y_pos[$idx] - $Y_pos[$idx-1] == -2) {
        $Y_pos[$idx]++;
      } elsif (abs($Y_pos[$idx] - $Y_pos[$idx-1]) <= 1) {
        # do nothing
      } else {
        printf("This should never happen - B [%d]!\n", $idx);
        DumpPosition();
        exit;
      }
    };

    # Otherwise, if the head and tail aren't touching 
    # and aren't in the same row or column, the tail always 

    # moves one step diagonally to keep up
    if (($X_pos[$idx] != $X_pos[$idx-1]) and ($Y_pos[$idx] != $Y_pos[$idx-1])) {

      # Following move is possible, only 'cause we've more than two elements
      if (abs($X_pos[$idx] - $X_pos[$idx-1]) + abs($Y_pos[$idx] - $Y_pos[$idx-1]) == 4) {
        if ($X_pos[$idx-1] - $X_pos[$idx] == -2) {
          $X_pos[$idx]--;
        } else {
          $X_pos[$idx]++;
        };
        if ($Y_pos[$idx-1] - $Y_pos[$idx] == -2) {
          $Y_pos[$idx]--;
        } else {
          $Y_pos[$idx]++;
        };
      };

      if (abs($X_pos[$idx] - $X_pos[$idx-1]) + abs($Y_pos[$idx] - $Y_pos[$idx-1]) > 2) {
        # they are too far. Need to move closer
        # print "mossa diagonale\n";

        if ($X_pos[$idx-1] - $X_pos[$idx] == -2) {
          $X_pos[$idx]--;
          $Y_pos[$idx] = $Y_pos[$idx-1];
        };
        if ($X_pos[$idx-1] - $X_pos[$idx] == 2) {
          $X_pos[$idx]++;
          $Y_pos[$idx] = $Y_pos[$idx-1];
        };

        if ($Y_pos[$idx-1] - $Y_pos[$idx] == -2) {
          $Y_pos[$idx]--;
          $X_pos[$idx] = $X_pos[$idx-1];
        };
        if ($Y_pos[$idx-1] - $Y_pos[$idx] == 2) {
          $Y_pos[$idx]++;
          $X_pos[$idx] = $X_pos[$idx-1];
        };
      } else {
        # they "touch". No need to move
      }
    }
    
    UpdateVisited($X_pos[9], $Y_pos[9]);
    # DumpPosition();
    # printf("Cur Status => H:[%3d;%3d] T:[%3d;%3d]\n",$X_pos[0], $Y_pos[0], $X_pos[9], $Y_pos[9]);
  }

};

sub DumpPosition() {
  print "---\nX: ";
  foreach my $idx (@X_pos) {
    printf("%5d",$idx);
  }
  print "\n";
  print "Y: ";
  foreach my $idx (@Y_pos) {
    printf("%5d",$idx);
  }
  print "\n";
}

