#!/usr/bin/perl

use strict;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

# to update visited locations
sub UpdateVisited($$);
sub UpdateTailPos();

# Initial Position
# Head
my $H_cur_x = 0;
my $H_cur_y = 0;
#
# Tail
my $T_cur_x = 0;
my $T_cur_y = 0;

# to hold teh visited coords
my %visited;

# Let's open the file for reading...
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");
while (my $line = <FH>) {

  # remove ending newline
  chomp($line);

  # Do we have a proper line (one char and one num)?
  if ($line =~ /^(\w)\s(\d+)$/) {
    # yes! Extract move and steps
    my ($move, $step) = ($1, $2);

    if ($move eq 'U') {
      foreach my $s (1..$step) {
        $H_cur_y--;
        UpdateTailPos();
      }
    } elsif ($move eq 'D') {
      foreach my $s (1..$step) {
        $H_cur_y++;
        UpdateTailPos();
      }
    } elsif ($move eq 'L') {
      foreach my $s (1..$step) {
        $H_cur_x--;
        UpdateTailPos();
      }
    } elsif ($move eq 'R') {
      foreach my $s (1..$step) {
        $H_cur_x++;
        UpdateTailPos();
      }
    } else {
      print "Bad move! $line\n";
      exit;
    }
  } else {
    print "Bad move! $line\n";
    exit;
  }

}

# print the score table, sorted by value
foreach my $k (sort {$visited{$a} <=> $visited{$b}} keys %visited) {
  printf("%s: [%d]\n", $k, $visited{$k});
}
print "Tot visited by tail: [" . scalar(keys %visited) . "]\n";
exit;

sub UpdateVisited($$) {
  my ($x, $y) = @_;

  my $pos_id = sprintf("%d;%d",  $x, $y);
  $visited{$pos_id}++;
}

sub UpdateTailPos() {
  # If the head is ever two steps directly up, down, left, or right 
  # from the tail, the tail must also move one step in that direction 
  # so it remains close enough:
  
  # Same Y / two step X
  if ($T_cur_y == $H_cur_y) {
    if ($T_cur_x - $H_cur_x == 2) {
      $T_cur_x--;
    } elsif ($T_cur_x - $H_cur_x == -2) {
      $T_cur_x++;
    } elsif (abs($T_cur_x - $H_cur_x) <= 1) {
      # do nothing
    } else {
      print "This should never happen - A!";
      printf("Cur Status => H:[%3d;%3d] T:[%3d;%3d]\n",$H_cur_x, $H_cur_y, $T_cur_x, $T_cur_y);
      exit;
    }
  };

  # Same X / two step Y
  if ($T_cur_x == $H_cur_x) {
    if ($T_cur_y - $H_cur_y == 2) {
      $T_cur_y--;
    } elsif ($T_cur_y - $H_cur_y == -2) {
      $T_cur_y++;
    } elsif (abs($T_cur_y - $H_cur_y) <= 1) {
      # do nothing
    } else {
      print "This should never happen - B!";
      printf("Cur Status => H:[%3d;%3d] T:[%3d;%3d]\n",$H_cur_x, $H_cur_y, $T_cur_x, $T_cur_y);
      exit;
    }
  };

  # Otherwise, if the head and tail aren't touching 
  # and aren't in the same row or column, the tail always 
  # moves one step diagonally to keep up
  if (($T_cur_x != $H_cur_x) and ($T_cur_y != $H_cur_y)) {
    if (abs($T_cur_x - $H_cur_x) + abs($T_cur_y - $H_cur_y) > 2) {
      # they are too far. Need to move closer
      print "mossa diagonale";

      if ($H_cur_x - $T_cur_x == -2) {
        $T_cur_x--;
        $T_cur_y = $H_cur_y;
      };
      if ($H_cur_x - $T_cur_x == 2) {
        $T_cur_x++;
        $T_cur_y = $H_cur_y;
      };

      if ($H_cur_y - $T_cur_y == -2) {
        $T_cur_y--;
        $T_cur_x = $H_cur_x;
      };
      if ($H_cur_y - $T_cur_y == 2) {
        $T_cur_y++;
        $T_cur_x = $H_cur_x;
      };
    } else {
      # they "touch". No need to move
    }
  }
  
  UpdateVisited($T_cur_x,$T_cur_y);

  printf("Cur Status => H:[%3d;%3d] T:[%3d;%3d]\n",$H_cur_x, $H_cur_y, $T_cur_x, $T_cur_y);

}
