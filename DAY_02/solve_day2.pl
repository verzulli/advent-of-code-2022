#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------
  
# Let's prepare the score map, telling which score we'll get
# if player 1 and player 2 choose related A|B|C and X|Y|Z
my %scoreMap = (
    'AX' => 4,
    'AY' => 8,
    'AZ' => 3,
    'BX' => 1,
    'BY' => 5,
    'BZ' => 9,
    'CX' => 7,
    'CY' => 2,
    'CZ' => 6
);

# scoreMap to calculate PART-Two values
my %scoreMapP2 = (
    'AX' => 3,
    'AY' => 4,
    'AZ' => 8,
    'BX' => 1,
    'BY' => 5,
    'BZ' => 9,
    'CX' => 2,
    'CY' => 6,
    'CZ' => 7
);

# Let's open the file for reading
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Let's initialize the temporary score SUM
my $totalScoreP1 = 0;
my $totalScoreP2 = 0;

# Let's read the file, line by line
while (my $round = <FH>) {

  # let's get rid of the terminating newline
  chomp($round);

  # let's uppercase the read line
  $round = uc($round);

  # Let's get rid of inner spaces
  $round =~ s/\s+//g;

  # Let's check the result score
  if (exists($scoreMap{$round})) {

    # add the result to global score
    $totalScoreP1 = $totalScoreP1 + $scoreMap{$round};
    $totalScoreP2 = $totalScoreP2 + $scoreMapP2{$round};

    printf("%s: [round: %d] [total: %d/%d]\n", $round, $scoreMap{$round}, $totalScoreP1, $totalScoreP2 ); 
  } else {
    # we got something strange so... let's throw away the line
    print "Ignoring bad line: [" . $round . "]\n"
  }
}

print "Total score P1: [" . $totalScoreP1 . "]\n";
print "Total score P2: [" . $totalScoreP2 . "]\n";