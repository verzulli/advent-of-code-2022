TL/DR;
======
We need to calculate the total score of 2.500 rounds of *"Rock, Paper, Scissors"* game. 

The score for each single round is the score for the shape you selected (1 for Rock, 2 for Paper, and 3 for Scissors) plus the score for the outcome of the round (0 if you lost, 3 if the round was a draw, and 6 if you won).


Original text
=============

    [...]
    The winner of the whole tournament is the player with the highest score. Your total score is the sum of your scores for each round. The score for a single round is the score for the shape you selected (1 for Rock, 2 for Paper, and 3 for Scissors) plus the score for the outcome of the round (0 if you lost, 3 if the round was a draw, and 6 if you won).

    Since you can't be sure if the Elf is trying to help you or trick you, you should calculate the score you would get if you were to follow the strategy guide.

    For example, suppose you were given the following strategy guide:

    A Y
    B X
    C Z

    This strategy guide predicts and recommends the following:

    - In the first round, your opponent will choose Rock (A),
      and you should choose Paper (Y). This ends in a win for 
      you with a score of 8 (2 because you chose Paper + 6 
      because you won).
    - In the second round, your opponent will choose Paper 
      (B), and you should choose Rock (X). This ends in a 
      loss for you with a score of 1 (1 + 0).
    - The third round is a draw with both players choosing 
      Scissors, giving you a score of 3 + 3 = 6.

    In this example, if you were to follow the strategy guide, you would get a total score of 15 (8 + 1 + 6).

    What would your total score be if everything goes exactly according to your strategy guide?

My notes
========
After a few tought, I saw that a simple matrix can easily show all possible round values, as shown below:

![](./assets/score_map_1.png)

Here you can see the score value for each combination:
* red cells => you loose;
* green cells => you win;
* yellow cells => draw.

So I plan to simply create an [HASH](https://www.perltutorial.org/perl-hash/), with the two characters as the *key* and the related score as the *value*. Sort of:

![](./assets/score_map_2.png)

In order to complete Part-Two, a different table is required:

![](./assets/score_map_3.png)

I'm going to implement the solution in [PERL](https://www.perl.org), as it's perfect to read input from files and to work properly with HASHes-

* My input data are in the "input.txt"
* My implemented solution is "solve_day2.pl"

Solving approach
----------------

* define the `%scoreMap` hash, containing score values;
* loop over the file
* apply some sanitization (remove spaces and termination newlines) to properly format the searching *key*;
* update the `$totalScore` with related score
* should something strange appear along the file, tell the user

Implementation
==============

Please refer to [`solve_day2.pl`](./solve_day2.pl)

Results
=======

    [verzulli@XPSGarr DAY_02]$ perl solve_day2.pl
    AY: [round: 8] [total: 8]
    BY: [round: 5] [total: 13]
    BZ: [round: 9] [total: 22]
    BZ: [round: 9] [total: 31]
    BX: [round: 1] [total: 32]
    [...]
    BX: [round: 1] [total: 15374]
    AZ: [round: 3] [total: 15377]
    BZ: [round: 9] [total: 15386]
    BZ: [round: 9] [total: 15395]
    BZ: [round: 9] [total: 15404]
    BZ: [round: 9] [total: 15413]
    BZ: [round: 9] [total: 15422]
    Total score: [15422]

Results - Part Two
==================

    [verzulli@XPSGarr DAY_02]$ perl solve_day2.pl | head -n 5
    AY: [round: 8] [total: 8/4]
    BY: [round: 5] [total: 13/9]
    BZ: [round: 9] [total: 22/18]
    BZ: [round: 9] [total: 31/27]
    BX: [round: 1] [total: 32/28]
    [...]
    BZ: [round: 9] [total: 15395/15415]
    BZ: [round: 9] [total: 15404/15424]
    BZ: [round: 9] [total: 15413/15433]
    BZ: [round: 9] [total: 15422/15442]
    Total score P1: [15422]
    Total score P2: [15442]
    [verzulli@XPSGarr DAY_02]$ 