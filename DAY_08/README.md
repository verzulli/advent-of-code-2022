TL/DR;
======
Being given a (big) matrix of single-cipher-numbers, representing a squared field of tree, with each number representing the heigth of related tree, find the number of the trees that are visibile from the outside of the field

Original text
=============

    [...]
    The Elves have already launched a quadcopter to generate a map with the height of each tree (your puzzle input). For example:

    30373
    25512
    65332
    33549
    35390

    Each tree is represented as a single digit whose value is its height, where 0 is the shortest and 9 is the tallest.

    A tree is visible if all of the other trees between it and an edge of the grid are shorter than it. Only consider trees in the same row or column; that is, only look up, down, left, or right from any given tree.

    All of the trees around the edge of the grid are visible - since they are already on the edge, there are no trees to block the view. In this example, that only leaves the interior nine trees to consider:

        The top-left 5 is visible from the left and top. (It isn't visible from the right or bottom since other trees of height 5 are in the way.)
        The top-middle 5 is visible from the top and right.
        The top-right 1 is not visible from any direction; for it to be visible, there would need to only be trees of height 0 between it and an edge.
        The left-middle 5 is visible, but only from the right.
        The center 3 is not visible from any direction; for it to be visible, there would need to be only trees of at most height 2 between it and an edge.
        The right-middle 3 is visible from the right.
        In the bottom row, the middle 5 is visible, but the 3 and 4 are not.

    With 16 trees visible on the edge and another 5 visible in the interior, a total of 21 trees are visible in this arrangement.

    Consider your map; how many trees are visible from outside the grid?

My notes
========
I'm going to implement the solution in [PERL](https://www.perl.org).

* My input data are in the "input.txt"
* My implemented solution is "solve_day8.pl"

Solving approach
----------------
* load everything in a matrix (`@field_map`);
* start considering visible all in the borders;
* let process line by line, the internal trees;
* for each tree, check the four directions and...;
* ...if visible, increase the counter;

Unfortunately, for P2, things are **VERY** different and sort of total rewrite have been required:
* load everything in a matrix (`@field_map`);
* throw away the borders, as they are scored 0;
* let process the matrix, line by line;
* for each tree, check the four directions and...;
* ...evaluate proper score and...
* ...calculate the total score, and track it.
* At the end, print the map of scores, ordered... and pick last value


Implementation
==============

Please:
* refer to [`solve_day8.pl`](./solve_day8.pl)

To check code for P1, refer to proper commit.

Results
=======

    [verzulli@XPSGarr DAY_08]$ perl solve_day8.pl 
    Field size: [98]x[98]
    Initially visible: [392]
    Checking ROW: [1]
    - Checking: [2][2] - [0]: ...NOT VISIBLE!
    - Checking: [2][3] - [0]: ...NOT VISIBLE!
    - Checking: [2][4] - [2]: ...VISIBLE - T!
    - Checking: [2][5] - [3]: ...VISIBLE - L!
    - Checking: [2][6] - [2]: ...NOT VISIBLE!
    - Checking: [2][7] - [3]: ...NOT VISIBLE!
    - Checking: [2][8] - [0]: ...NOT VISIBLE!
    [...]
    - Checking: [98][91] - [0]: ...NOT VISIBLE!
    - Checking: [98][92] - [2]: ...VISIBLE - B!
    - Checking: [98][93] - [3]: ...VISIBLE - B!
    - Checking: [98][94] - [3]: ...VISIBLE - B!
    - Checking: [98][95] - [1]: ...NOT VISIBLE!
    - Checking: [98][96] - [1]: ...VISIBLE - B!
    - Checking: [98][97] - [3]: ...VISIBLE - B!
    - Checking: [98][98] - [3]: ...VISIBLE - R!

    Finally visible: [1693]

Results for P2
==============

    [verzulli@XPSGarr DAY_08]$ perl solve_day8.pl   | head -n 15
    Field size: [98]x[98]
    Checking ROW: [1]
    - Checking: [1][1] - [0]:
    Score L: [1]
    Score R: [1]
    Score T: [1]
    Score B: [1]

    - Checking: [1][2] - [0]:
    Score L: [1]
    Score R: [1]
    Score T: [1]
    Score B: [1]

    - Checking: [1][3] - [2]:
    [...]
    - Checking: [97][97] - [3]:
    Score L: [1]
    Score RT: [1]
    Score T: [3]
    Score B: [1]

    1; [   20;   67]
    1; [   92;   56]
    1; [   65;   56]
    1; [    5;   66]
    1; [   65;   33]
    1; [   25;   31]
    1; [   43;   54]
    1; [   70;   31]
    [...]
    134232; [   14;   51]
    136136; [   28;   64]
    148176; [   77;   56]
    148428; [   80;   31]
    149184; [   37;   32]
    183600; [   34;   30]
    251160; [   15;   52]
    266084; [   72;   43]
    308880; [   65;   89]
    422059; [   17;   61]
