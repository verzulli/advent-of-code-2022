#!/usr/bin/perl

use strict;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

# the map of the field. Technically, an Array (rows) of Array_Refs (columns)
my @field_map = ();

# let's store the size of the field
# base
my $h = 0;
my $l = 0;

# to hold score values
my %score;

# Let's open the file for reading and populate the in-memory field map
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");
while (my $line = <FH>) {

  # remove ending newline
  chomp($line);

  #split the line and populate the array
  my @row_list = split(//, $line); 
  push(@field_map, \@row_list);  
}

$h = $#field_map;
my @t = @{$field_map[0]};
$l = $#t;

printf("Field size: [%d]x[%d]\n",$h,$l);

# Let process visibility... row by row
foreach my $row_idx (1..$h-1) {

  # extract the row
  my @row = @{$field_map[$row_idx]};

  printf("Checking ROW: [%d]\n", $row_idx);

  foreach my $col_idx (1..$#row-1) {

    # if we're here... the tree is vi
    printf("- Checking: [%d][%d] - [%d]:\n", $row_idx, $col_idx, $field_map[$row_idx]->[$col_idx]);

    # need to check ($row_idx, $col_idx)
    my $cur_height = $field_map[$row_idx]->[$col_idx];

    # Let's initialize the score (1)
    my $global_score = 1;
    my $local_score = 1;
    my $terminate = 0;

    # ################################################
    # search from left to POS
    for(my $i = $col_idx-1; $i>=0; $i--) {

      #debug
      # printf("H(%d;%d)=%d\n", $row_idx, $i, $field_map[$row_idx]->[$i]);

      if ($field_map[$row_idx]->[$i]>=$cur_height) {
        $local_score = $col_idx - $i;
        printf("Score L: [%d]\n", $local_score);
        $terminate=1;
        last;
      }
    }
    if (!$terminate) {
      $local_score = $col_idx;
      printf("Score LT: [%d]\n", $local_score);
    }
    $global_score = $global_score * $local_score;

    # ################################################
    # search from POS to right
    $terminate=0;
    $local_score = 1;
    for (my $i=$col_idx+1; $i <= $h; $i++) {

      #debug
      # printf("H(%d;%d)=%d\n", $row_idx, $i, $field_map[$row_idx]->[$i]);

      if ($field_map[$row_idx]->[$i]>=$cur_height) {
        $local_score = $i - $col_idx;
        printf("Score R: [%d]\n", $local_score);
        $terminate=1;
        last;
      }
    }
    if (!$terminate) {
      $local_score = $h - $col_idx;
      printf("Score RT: [%d]\n", $local_score);
    }
    $global_score = $global_score * $local_score;

    # ################################################
    # search from POS to top
    $terminate = 0;
    $local_score = 1;
    for(my $i = $row_idx-1; $i>=0; $i--) {

      #debug
      # printf("H(%d;%d)=%d\n", $i, $col_idx, $field_map[$i]->[$col_idx]);

      if ($field_map[$i]->[$col_idx]>=$cur_height) {
        $local_score = $row_idx - $i;
        printf("Score T: [%d]\n", $local_score);
        $terminate = 1;
        last;
      }
    }
    if (!$terminate) {
      $local_score = $row_idx;
      printf("Score TT: [%d]\n", $local_score);
    }
    $global_score = $global_score * $local_score;

    # ################################################
    # search from POS to bottom
    $terminate=0;
    $local_score = 1;
    for (my $i=$row_idx+1; $i <= $h; $i++) {

      #debug
      # printf("H(%d;%d)=%d\n", $i, $col_idx, $field_map[$i]->[$col_idx]);

      if ($field_map[$i]->[$col_idx]>=$cur_height) {
        $local_score = $i - $row_idx;
        printf("Score B: [%d]\n", $local_score);
        $terminate=1;
        last;
      }
    }
    if (!$terminate) {
      $local_score = $h - $row_idx;
      printf("Score BT: [%d]\n", $local_score);
    }
    $global_score = $global_score * $local_score;

    print "\n";

    # Let's store the score
    my $key = sprintf("%5d;%5d", $row_idx, $col_idx);
    $score{$key} = $global_score;
  }
}

# print the score table, sorted by value
foreach my $k (sort {$score{$a} <=> $score{$b}} keys %score) {
  printf("%d; [%s]\n", $score{$k}, $k);
}
exit;

