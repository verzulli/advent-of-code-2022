#!/usr/bin/perl

use strict;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

sub GetPos($);

# Let's open the file for reading
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

my $saved_line;
my $cur_folder;

# a STACK (an array) keeping track of current position 
# in the file-system
my @folder_stack = ();

# an HASH to keep track of folder size values
my %folder_sizes = ();

# to keep track of looping over main file
my $continue_main_loop = 1;

while ( $continue_main_loop ) {

  # the line to be processed
  my $line;

  # am I processing an already-read line?
  if ($saved_line) {
    printf("* Processing saved_line [%s]\n",$saved_line);
    $line = $saved_line;
    undef $saved_line;
  } else {
    $line = <FH>;
    last if (!$line);
  };

  # Let's process the "cd <something>"
  if ($line =~ /^\$\scd\s\.\.$/) {
    printf("exiting folder\n");
    $cur_folder = pop(@folder_stack);
    printf("cur pos: [%s]\n", GetPos(\@folder_stack));
    next;
  } elsif ($line =~ /^\$\scd\s(.+)$/) {
    # entering folder $1
    $cur_folder = $1;
    
    # let's rename "root", to easy program output
    $cur_folder = 'ROOT' if ($cur_folder eq '/');
    
    printf("entering folder: [%s]\n",$cur_folder);
    push(@folder_stack, $cur_folder);
    printf("cur pos: [%s]\n", GetPos(\@folder_stack));
    next;
  };

  # Let's process the "ls"
  if ($line =~ /^\$\sls$/) {
    printf("print content of folder [%s]\n", $cur_folder);

    # process the output, line by line, up to next command (line starting with $)
    # Of course, we should keep track of such "$ ..." line, as it needs to be processed
    # by the outer loop (as we read it, but it's not the output of "ls")
    while (my $output = <FH>) {
      chomp ($output);
      # If the list finished, let's keep track of the line/command just read
      if ($output =~ /^\$/) {
        $saved_line = $output;
        # ...and move back to outer loop
        last;
      }
      # Let's process the line of output
      if ($output =~ /^dir\s(.+)$/) {
        my $folder_name = $1;
        printf("- [DIR ] [.......] [%s]\n",$folder_name);
      } elsif ($output =~ /^(\d+)\s(.+)$/) {
        my $file_size = $1;
        my $file_name = $2;
        my $file_full_path = GetPos(\@folder_stack);
        printf("- [FILE] [%7d] [%s] [%s]\n",$file_size, $file_full_path, $file_name);

        # update the folder size total (...along all of the tree)
        my @tmp_folder_stack = ();
        foreach my $folder (@folder_stack) {
          push (@tmp_folder_stack, $folder);
          my $tmp_path = GetPos(\@tmp_folder_stack);
          $folder_sizes{$tmp_path} += $file_size;
        }

      } else {
        printf("- [UNKNOWN LINE] [%s]\n",$output);
      }
    }
  }
}
# Calculate results
my $p1_total = 0;
foreach my $f (sort keys %folder_sizes) {
  if ($folder_sizes{$f} <= 100000) {
    $p1_total += $folder_sizes{$f};
    printf("[%10d] [%s]\n", $folder_sizes{$f}, $f);
  }
}
print "P1 total: [" . $p1_total . "]\n";

# Let's start working for P2
print "TOTAL...: [" . $folder_sizes{'ROOT'} . "]\n";

# get the free space
my $cur_free = 70000000 - $folder_sizes{'ROOT'};
print "CUR FREE: [" . $cur_free . "]\n";

my $need_to_free = 30000000 - $cur_free;
print "TO FREE.: [" . $need_to_free . "]\n";

# let's loop over the folders, ordered by size.. and find the first "good"
foreach my $f ( sort {$folder_sizes{$a} <=> $folder_sizes{$b}} keys %folder_sizes) {
  if ($folder_sizes{$f} >= $need_to_free) {
    printf("TO BE DELETED: [%10d] [%s]\n", $folder_sizes{$f}, $f);
    last;
  }
}
exit;

sub GetPos($) {
  my ($aRef) = @_;

  my $str = join('/',@{$aRef});
  #$str =~ s/^\/\//\//;
  return $str;
}
