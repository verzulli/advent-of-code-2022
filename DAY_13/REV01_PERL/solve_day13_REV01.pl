#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

# NON è 5301
# NON è 5694 (troppo alta)
use strict;
use Data::Dumper;
use Term::Cap;

$|=1;

my $verbose = 1;
sub Trace($);
sub GetNextItem($);

# Let's open the file for reading...
open(FH, "../input.txt") || die ("Unable to open input.txt: $!");

my $block_id = 0;
my $block_ref = {};

# Let's load the data and populate the in-memory data structure (%monkeys)...
while (my $line_01 = <FH>) {

  $block_id++;

  # Process LEFT
  # - remove ending newline and group delimiters - for LEFT
  chomp($line_01);
  my $line_left = $line_01;
  $line_01 =~ s/^\[//;
  $line_01 =~ s/\]$//;

  #split the line and populate the array
  my @list_left = split(//, $line_01);

  # Process RIGHT, the same way
  my $line_02 = <FH>;
  chomp($line_02);
  my $line_right = $line_02;
  $line_02 =~ s/^\[//;
  $line_02 =~ s/\]$//;
  my @list_right = split(//, $line_02);

  my $this_block = {
    'left' => \@list_left,
    'right' => \@list_right,
    'line_left' => $line_left,
    'line_right' => $line_right
  };
  $block_ref->{$block_id} = $this_block;
  my $null = <FH>;
}

# Dump blocks:
foreach my $b (sort {$a <=> $b} keys %{$block_ref}) {
  Trace("\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  Trace(sprintf("Processing BLOCK >%s<\n",$b));
  Trace(sprintf("LEFT : >>>%s<<<\n", join('_',@{$block_ref->{$b}{'left'}})));
  Trace(sprintf("RIGHT: >>>%s<<<\n", join('_',@{$block_ref->{$b}{'right'}})));

  my $result = ProcessBlock($block_ref->{$b});
  $block_ref->{$b}{'ordered'} = $result;
}

# Dump blocks:
my $sum = 0;
foreach my $b (sort {$a <=> $b} keys %{$block_ref}) {
  my $status = 'UNORDERED';
  my $adding = '';
  if ($block_ref->{$b}{'ordered'}) {
    $status = 'ORDERED';
    $adding = '...ADDING!';
    $sum = $sum + $b
  };
  printf("LEFT : >>>%s<<<\n", $block_ref->{$b}{'line_left'});
  printf("RIGHT: >>>%s<<<\n", $block_ref->{$b}{'line_right'});
  printf("BLOCK >%s< is >%s< %s\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",$b, $status, $adding);
};
print "TOTAL_SUM: [".$sum."]\n";
exit;

sub ProcessBlock ($) {
  my ($block) = @_;

  # Loop over items, one by one
  my $ordered;
  my $continue_searching = 1;
  do {
    # Fetch Next x LEFT
    my $left_as_string = GetNextItem($block->{'left'});
    Trace(sprintf("\tLeft Next: >%s<\n", $left_as_string));

    # Fetch Next x RIGHT
    my $right_as_string = GetNextItem($block->{'right'});
    Trace(sprintf("\tRight Next: >%s<\n", $right_as_string));

    # if RIGHT is missing (runs out, before left), than set unordered
    if ($right_as_string && !$left_as_string) {
      # left runs out before right. Set ORDERED
      $ordered = 1;
      $continue_searching = 0;
      printf("LEFT runs out before RIGHT\n");
      Trace("Block is ORDERED - Reason 3\n");
      return $ordered;
    } elsif ($left_as_string && !$right_as_string) {
      # right runs out before left. Set UNORDERED
      $ordered = 0;
      $continue_searching = 0;
      printf("RIGHT runs out before LEFT\n");
      Trace("Block is UNORDERED - Reason 4\n");
      return $ordered;
    } elsif (!$left_as_string && !$right_as_string) {
      # left and right runs out at the same time. Proceed
      $continue_searching = 0;
      $ordered = 1;
      printf("LEFT and RIGHT runs out together. LEFT wins! Ordered!\n");
      Trace("Block is ORDERED - Reason 5\n");
      return $ordered;
    } else {
      # both values are "number"s
      if ($left_as_string =~ /^\d+$/ && $right_as_string =~ /^\d+$/) {
        if ($left_as_string < $right_as_string ) {
          $ordered = 1;
          $continue_searching = 0;
          Trace("Block is ORDERED - Reason 1\n");
          return $ordered;
        }
        if ($left_as_string > $right_as_string ) {
          $ordered = 0;
          $continue_searching = 0;
          Trace("Block is UNORDERED - Reason 2\n");
          return $ordered;
        }
      } elsif ($left_as_string !~ /^\d+$/ && $right_as_string !~ /^\d+$/){
        # both values are NOT "number"s; aka: both are LIST

        # TODO
        $continue_searching = 0;
        printf("Need to process two list L:>%s< and R:>%s<\n",$left_as_string, $right_as_string);
        my $sub_block = {
          'left' => [],
          'right' => [],
        };

        @{$sub_block->{'left'}} = split(//, $left_as_string);
        @{$sub_block->{'right'}} = split(//, $right_as_string);
        Trace(sprintf("Processing SUBBLOCK\n"));
        Trace(sprintf("LEFT : >>>%s<<<\n", join('_',@{$sub_block->{'left'}})));
        Trace(sprintf("RIGHT: >>>%s<<<\n", join('_',@{$sub_block->{'right'}})));
        $ordered = ProcessBlock($sub_block);
        $continue_searching = 0;
      } else {
        # one value is "number"; the other is list

        printf("Need to process num/list L:>%s< and R:>%s<\n",$left_as_string, $right_as_string);
        $continue_searching = 0;
        # TODO
        my $sub_block = {
          'left' => [],
          'right' => [],
        };

        @{$sub_block->{'left'}} = split(//, $left_as_string);
        @{$sub_block->{'right'}} = split(//, $right_as_string);
        Trace(sprintf("Processing SUBBLOCK\n"));
        Trace(sprintf("LEFT : >>>%s<<<\n", join('_',@{$sub_block->{'left'}})));
        Trace(sprintf("RIGHT: >>>%s<<<\n", join('_',@{$sub_block->{'right'}})));
        $ordered = ProcessBlock($sub_block);
        $continue_searching = 0;
      }
    }
  } while ($continue_searching);
  return $ordered;
};

sub GetNextItem($) {
  my ($list_ref) = @_;

  # Trace("Got: ==>" . join("-",@{$list_ref}) . "<==\n");
  my $current_depth_level = 0;

  my $item_as_string = '';

  # Let's process the raw items, one char at a time
  my $next_char = shift (@{$list_ref});

  # Trace(sprintf("Read char >%s<\n",$next_char));

  # It's a beginning of a sub-list?  
  if ($next_char eq '[') {
    # yes... Let's retrieve the whole list
  
    # Trace("parsing group...\n");

    $current_depth_level++;

    # we need manual handling of loop, due to the "0" values
    # that could trigger a wrongly exit
    my $inner_loop = 1;
    # fetch up to proper ']'
    while ($inner_loop) {
      my $inner_char = shift (@{$list_ref});
      last if (!defined($inner_char));

      # Trace(sprintf("Group: got >%s< inner char\n", $inner_char));
      if ($inner_char eq '[') {
        $current_depth_level++;
      } elsif ($inner_char eq ']') {
        $current_depth_level--;
      };
      # next should be a comma, as we exited from a block...
      if ($current_depth_level == 0) {
        my $comma = shift (@{$list_ref});
        if (defined($comma) && ($comma ne ',')) {
          printf("Error: expecting comma, but got >%s<. Terminating!\n", $comma);
          exit;
        } else {
          printf("Warning 1: I'm here... but don't know what...\n");
          last;
        }
      } else {
        $item_as_string = $item_as_string . $inner_char;
      }
    }
    printf("Warning 2: Out of inner loop 1...\n");
  } elsif ($next_char =~ /\d/) {
    # we got a number. We expect additional numbers, or comma
    $item_as_string = $next_char;

    my $inner_loop = 1;
    while ($inner_loop) {
      my $following_char = shift (@{$list_ref});
      last if (!defined($following_char));
      last if ($following_char eq ',');
      if ($following_char =~ /\d/) {
        $item_as_string = $item_as_string . $following_char;
      } else {
        printf("Error: expecting number or comma, but got >%s<. Terminating!\n", $following_char);
        exit;
      }
    }
    printf("Warning 3: Out of inner loop 2...\n");
  } elsif (!$next_char) {
    # print "Got NO item (empty list!)\n";
    $item_as_string = undef;
  } else {
    printf("Don't know what! Got >%s<!\n", $next_char);
  };
  Trace(sprintf("[GetNextItem] Got ITEM: >>%s<<\n",$item_as_string));
  return $item_as_string;
}
  
exit;

sub Trace($) {
  my ($msg) = @_;

  print $msg if $verbose;
}