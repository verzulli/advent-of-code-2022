const winston = require('winston')

const myFormat = winston.format.printf(
  ({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
  }
);

const logger = winston.createLogger({
  level: 'debug',
  format: myFormat
});

logger.add(new winston.transports.Console({
  format: winston.format.combine(
    winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss.SSS' }),
    winston.format.label({ label: 'AoC2022_DAY13' }),
    myFormat
  )
}))

export default logger
