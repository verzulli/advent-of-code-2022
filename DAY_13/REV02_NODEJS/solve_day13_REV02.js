import logger from './components/logger'
import fs from 'fs'
import readline from 'readline'
import {sprintf} from 'sprintf-js'

let inputCSV = "../input.txt"

let blocks = []

let recursionDepth = 0

function dumpBlocks() {
  logger.info("[dumpBlocks] Start loading...")

  for (let index = 0; index < blocks.length; index++) {
    const b = blocks[index];
    logger.info(sprintf("block [%d]: [%s]\n\tLeft >>>%s<<<\n\tRight: >>>%s<<<\n\n", b.id, index, JSON.stringify(b.left), JSON.stringify(b.right)))
  }
}

// Carica il CSV nel DB. Occhio che è asincrona... quindi ci organizziamo per
// farla diventare sincrona (con una "promise")
async function loadInput() {
  logger.info("[loadInput] Start loading...")
  let inputNumBlocks = 0

  // questo è lo stream dal quale "leggiamo"... il file che arriva (a chunk)
  const fileReadStream = fs.createReadStream(inputCSV)

  fileReadStream.on('close', () => {
    logger.info("[fileReadStream - onClose] Fine dello stream!!")
  })

  fileReadStream.on('error', (e) => {
    logger.error("[fileReadStream - onError] Errore dello stream del file: [" + e.message + "]")
  })

  // rl è il "filtro" che legge i chunk in arrivo da fileReadStream e quando
  // c'e' una riga sana, spara l'evento "line" (con la riga)
  const rl = readline.createInterface({
    input: fileReadStream,
    terminal: false
  })

  // quando finisce il file, me lo segno...
  rl.on('close', () => {
    logger.info("[rl - onClose] Fine dello stream!!")
  })

  // ----------------------------------------------------
  // Qui comincia l'elaborazione vera e propria del file
  //
  let firstLine = true
  let secondLine = false

  // mi segno che ora è (cosi' posso calcolare quanto dura l'operazione)
  let tsStart = Date.now()

  let leftList = []
  let rightList = []
  // normalmente sarebbe "rl.on('line', (line) => {...})" ma a noi serve sincrona
  // quindi andiamo di async/await
  for await (const line of rl) {

    try {                

      // a new group just started...
      if (line === '') {
        inputNumBlocks++
        firstLine = true
        secondLine = false
        leftList = []
        rightList = []
      } else if (firstLine) {
        leftList=JSON.parse(line)
        firstLine = false
        secondLine = true
      } else if (secondLine) {
        rightList=JSON.parse(line)

        let thisBlock = {
          'id': inputNumBlocks+1,
          'left': leftList,
          'right': rightList
        }
        // we just got 2nd line. So let's process the group
        blocks[inputNumBlocks] = thisBlock

        firstLine = false
        secondLine = false        

      } else {
        logger.info("Something wrong happened...")
      }

    } catch (err) {
      logger.error('[loadInput] Error [' + err.message + '] - line: (' + line + ')')
    } 
  }

  logger.info('[loadInput] read [' + inputNumBlocks + '] records')
}

function process(leftList, rightList) {
  let ordered = undefined
  let loop = true;
  while (loop) {
    let leftItem = leftList.shift()
    let rightItem = rightList.shift()

    // logger.info(sprintf("[process] checking Left: >>>%s/%s<<< - Right: >>>%s/%s<<<", leftItem, JSON.stringify(leftList), rightItem, JSON.stringify(rightList)))
    if (typeof(leftItem)==='undefined' && typeof(rightItem)==='undefined') {
      // BOTH got emptied. Let's continue...
      loop = false
    } else if (typeof(leftItem)==='undefined' && typeof(rightItem)!=='undefined') {
      // LEFT finished, before RIGHT. ORDERED.
      loop = false
      ordered = true
    } else if (typeof(leftItem)!=='undefined' && typeof(rightItem)==='undefined') {
      // RIGH finished, before LEFT. UNORDERED.
      loop = false
      ordered = false
    } else if (typeof(leftItem) === 'number' && typeof (rightItem) === 'number') {
      // BOTH are numbers
      if (leftItem < rightItem) {
        ordered = true
        loop = false
      } else if (leftItem > rightItem) {
        ordered = false
        loop = false
      }
    } else if (Array.isArray(leftItem) && Array.isArray(rightItem)) {
      // BOTH LISTS. Let's "recurse"
      ordered = process(leftItem, rightItem)
      if (typeof(ordered)==='undefined') {
        loop = true
      } else {
        loop = false
      }
    } else if (Array.isArray(leftItem) && typeof(rightItem)==='number') {
      // LEFT is LIST, RIGHT is number
      ordered = process(leftItem, [ rightItem ])
      if (typeof(ordered)==='undefined') {
        loop = true
      } else {
        loop = false
      }
    } else if (typeof(leftItem)==='number' && Array.isArray(rightItem)) {
      // LEFT is Number, RIGHT is LIST
      ordered = process([ leftItem ], rightItem)
      if (typeof(ordered)==='undefined') {
        loop = true
      } else {
        loop = false
      }
    }
  }
  return ordered
}

async function run () {
  await loadInput();
  // dumpBlocks()

  let sum = 0;
  for (let idx = 0; idx < blocks.length; idx++) {
    const b = blocks[idx]
    logger.info(sprintf("[process] Processing Block %d: >>>%s<<< - Right: >>>%s<<<", b.id, JSON.stringify(b.left), JSON.stringify(b.right)))
    let res = process(b.left, b.right)
    if (res) { sum += b.id}
    logger.info(sprintf("[MAIN]: BLOCK [%d] is: [%s]", b.id, res))
  } 
  logger.info("SUM: [" + sum + "]")
}

logger.info('[MAIN] Starting PROCESSOR....')
run();
