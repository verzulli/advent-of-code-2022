import logger from './components/logger'
import fs from 'fs'
import readline from 'readline'
import {sprintf} from 'sprintf-js'

let inputCSV = "../input_test.txt"

let blocks = []

let recursionDepth = 0

function dumpBlocks() {
  logger.info("[dumpBlocks] Start loading...")

  for (let index = 0; index < blocks.length; index++) {
    const b = blocks[index];
    logger.info(sprintf("block [%d]: [%s]\n\tLeft >>>%s<<<\n\tRight: >>>%s<<<\n\n", b.id, index, JSON.stringify(b.left), JSON.stringify(b.right)))
  }
}

// Carica il CSV nel DB. Occhio che è asincrona... quindi ci organizziamo per
// farla diventare sincrona (con una "promise")
async function loadInput() {
  logger.info("[loadInput] Start loading...")
  let inputNumBlocks = 1

  // questo è lo stream dal quale "leggiamo"... il file che arriva (a chunk)
  const fileReadStream = fs.createReadStream(inputCSV)

  fileReadStream.on('close', () => {
    logger.info("[fileReadStream - onClose] Fine dello stream!!")
  })

  fileReadStream.on('error', (e) => {
    logger.error("[fileReadStream - onError] Errore dello stream del file: [" + e.message + "]")
  })

  // rl è il "filtro" che legge i chunk in arrivo da fileReadStream e quando
  // c'e' una riga sana, spara l'evento "line" (con la riga)
  const rl = readline.createInterface({
    input: fileReadStream,
    terminal: false
  })

  // quando finisce il file, me lo segno...
  rl.on('close', () => {
    logger.info("[rl - onClose] Fine dello stream!!")
  })

  // ----------------------------------------------------
  // Qui comincia l'elaborazione vera e propria del file
  //

  // normalmente sarebbe "rl.on('line', (line) => {...})" ma a noi serve sincrona
  // quindi andiamo di async/await
  for await (const line of rl) {

    try {                

      // logger.info(sprintf("[loadInput] GOT: >>>%s<<<", line))
      // a new group just started...
      if (typeof(line) !== 'undefined' && line !== '') {
        let b = {
          'id': inputNumBlocks,
          'txt': line,
          'line': JSON.parse(line),
          'seq': 0
        }
        // we just got a line
        blocks[inputNumBlocks] = b

        // logger.info(sprintf("[loadInput] Loaded [%d] >>>%s<<<", inputNumBlocks, JSON.stringify(blocks[inputNumBlocks])))
        inputNumBlocks++  
      }

    } catch (err) {
      logger.error('[loadInput] Error [' + err.message + '] - line: (' + line + ')')
    } 
  }
  // Adding  [[2]] and [[6]]
  blocks[inputNumBlocks] = {
    'id': inputNumBlocks,
    'txt': '[[2]]',
    'line': [[2]],
    'seq': 0
  }
  inputNumBlocks++

  blocks[inputNumBlocks] = {
    'id': inputNumBlocks,
    'txt': '[[6]]',
    'line': [[6]],
    'seq': 0
  }

  logger.info('[loadInput] read [' + inputNumBlocks + '] records')
}

function process(leftList, rightList) {
  let ordered = undefined
  let loop = true;
  while (loop) {
    let leftItem = leftList.shift()
    let rightItem = rightList.shift()

    // logger.info(sprintf("[process] checking Left: >>>%s/%s<<< - Right: >>>%s/%s<<<", leftItem, JSON.stringify(leftList), rightItem, JSON.stringify(rightList)))
    if (typeof(leftItem)==='undefined' && typeof(rightItem)==='undefined') {
      // BOTH got emptied. Let's continue...
      loop = false
    } else if (typeof(leftItem)==='undefined' && typeof(rightItem)!=='undefined') {
      // LEFT finished, before RIGHT. ORDERED.
      loop = false
      ordered = true
    } else if (typeof(leftItem)!=='undefined' && typeof(rightItem)==='undefined') {
      // RIGH finished, before LEFT. UNORDERED.
      logger.info("RIGHT finished, before LEFT. UNORDERED")
      loop = false
      ordered = false
    } else if (typeof(leftItem) === 'number' && typeof (rightItem) === 'number') {
      // BOTH are numbers
      if (leftItem < rightItem) {
        ordered = true
        loop = false
      } else if (leftItem > rightItem) {
        ordered = false
        loop = false
      }
    } else if (Array.isArray(leftItem) && Array.isArray(rightItem)) {
      // BOTH LISTS. Let's "recurse"
      ordered = process(leftItem, rightItem)
      if (typeof(ordered)==='undefined') {
        loop = true
      } else {
        loop = false
      }
    } else if (Array.isArray(leftItem) && typeof(rightItem)==='number') {
      // LEFT is LIST, RIGHT is number
      ordered = process(leftItem, [ rightItem ])
      if (typeof(ordered)==='undefined') {
        loop = true
      } else {
        loop = false
      }
    } else if (typeof(leftItem)==='number' && Array.isArray(rightItem)) {
      // LEFT is Number, RIGHT is LIST
      ordered = process([ leftItem ], rightItem)
      if (typeof(ordered)==='undefined') {
        loop = true
      } else {
        loop = false
      }
    }
  }
  return ordered
}

async function run () {
  await loadInput();
  // dumpBlocks()

  for (const idx of Object.keys(blocks)) {
    logger.info(sprintf("[process] Block [%d] >>>%s<<<", idx, JSON.stringify(blocks[idx].line)))
    blocks[idx].seq=idx;
    // let res = process(b.left, b.right)
  } 

  let indexes = Object.keys(blocks)
  let sorted_indexes = Object.keys(blocks)
  let first = {}
  let second = {}

  for (let i=0; i<indexes.length-1; i++) {

    first = JSON.parse(JSON.stringify(blocks[indexes[i]]))
    for (let j=i+1; j<indexes.length; j++) {
      second = JSON.parse(JSON.stringify(blocks[indexes[j]]))

      let msg = sprintf("[run/final] DEBUG: Comparing [%d]/[%d] => >>>%s<<< / >>>%s<<<...:", first.id, second.id, JSON.stringify(blocks[indexes[i]].line), JSON.stringify(blocks[indexes[j]].line))

      let res = process(first.line, second.line)
      msg = msg + ' - RES:[' + res + ']'
      // logger.info(msg)
      if (!res) {
        // wrong order: let's swap

        logger.info(sprintf("Need to swap [%d/%d] => >>>%s<<< >>>%s<<<",i,j,JSON.stringify(blocks[indexes[i]].line),JSON.stringify(blocks[indexes[j]].line)))
        
        // need to swap i and j
        let tmp = sorted_indexes[i]
        sorted_indexes[i] = sorted_indexes[j]
        sorted_indexes[j] = tmp
      } else {
        logger.info(sprintf("Need to NOT swap [%d/%d] => >>>%s<<< >>>%s<<<",i,j,JSON.stringify(blocks[indexes[i]].line),JSON.stringify(blocks[indexes[j]].line)))
      }
    }
  }       

  for (const idx of sorted_indexes) {
    logger.info(sprintf("[process] Block [%d] >>>%s<<<", blocks[idx].seq, JSON.stringify(blocks[idx].line)))
    // let res = process(b.left, b.right)
  } 
  
}

logger.info('[MAIN] Starting PROCESSOR....')
run();
