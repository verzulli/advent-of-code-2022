TL/DR;
======
We're given a long list of "blocks", with every block as a couple of two strings representing arrays (of nested arrays, or integers).
We're given an algorithm to check if that couple are in the right order... or not.
You need to check/count which couples are in the right order.

Original text
=============

Better to refer to original text, [here](https://adventofcode.com/2022/day/13).


Solving approach
================

Phase 1 - PERL- **FAILED**!
---------------------------

I started writing a PERL-parser (see [here](REV01_PERL/solve_day13_REV01.pl)), creating an in-memory structure (an HASH of HASHes) to easy the comparison.

Several interesting problems arised: empty arrays (to be checked, as for the give algorithm), zeros management (zeros are valid items, and they need to be checked as "true" values, in my implementation), and others.

I **failed** several times in passing the "test" list (aka: running my algorithm with the test input provided in the original TEXT of DAY_13) and so... decided to play a bit harder, with [NodeJS](https://nodejs.org/en/).

The switch from Perl to Node was mainly due 'cause each block-element, basically, is a valid JSON and... if you need to play with JSONs, the natural fit is something Javascript based (in case you miss it, the "J" in "JSON" stand for "Javascript"). As I'm comfortable with Node... I switched to it.

Phase 2 - NodeJS - P1 - **SUCCESS**
-----------------------------------
I easily scrached down [solution for P1](REV02_NODEJS/solve_day13_REV02.js), based on the same approach I used with PERL
As expected, transforming JSON in in-memory structures, has been easy.
Actually, the very same PERL approach --that failed, when run in PERL-- was succesfull, when run in Node.
That means that for sure I left some bug, somewhere, in my PERL implementation.

Phase 2 - NodeJS - P2 - **FAIL**
--------------------------------
Switching from P1 to P2 should have been trivial, as P2 simply required to create an ordered list of input-lines... so, in short, you need simply to compare each couple (or, if you're brave enough, to implement some smarter sort-algorithm).

Even tough I choose the not-optimal path (aka: compare every possible couple)... I have been *UN*succesfull.

I tried a bit in fixing the stuff but... at the end, as Christmas arrived... I decided to stop here.

So, P2 is still broken!


Implementation
==============

Please:
* refer to [`REV01_PERL/solve_day13_REV01.pl`](./REV01_PERL/solve_day13_REV01.pl) for my first **FAILED** attempt to solve P1;
* refer to [`REV02_NODEJS/solve_day13_REV02.js`](./REV02_NODEJS/solve_day13_REV02.js) for my second **SUCCESFULL** attempt to solve P1;
* refer to [`REV02_NODEJS/solve_day13_P2.js`](./REV02_NODEJS/solve_day13_P2.js) for my first **FAILED** attempt to solve P2

Results
=======

* PERL - P1 (failed, as 5694 is WRONG!)

~~~
[verzulli@XPSGarr REV01_PERL]$ perl solve_day13_REV01.pl


~~
Processing BLOCK >1<
LEFT : >>>[_6_,_[_[_0_]_,_[_6_,_7_]_,_[_4_,_1_0_,_8_,_4_,_1_]_]_,_1_,_[_[_2_,_6_]_,_0_,_[_4_,_6_,_2_,_3_]_,_9_]_]<<<
RIGHT: >>>[_1_0_,_[_7_]_,_6_]<<<
Warning 1: I'm here... but don't know what...
Warning 2: Out of inner loop 1...
[GetNextItem] Got ITEM: >>6,[[0],[6,7],[4,10,8,4,1]],1,[[2,6],0,[4,6,2,3],9]<<
    Left Next: >6,[[0],[6,7],[4,10,8,4,1]],1,[[2,6],0,[4,6,2,3],9]<
Warning 1: I'm here... but don't know what...
Warning 2: Out of inner loop 1...
[GetNextItem] Got ITEM: >>10,[7],6<<
    Right Next: >10,[7],6<
Need to process two list L:>6,[[0],[6,7],[4,10,8,4,1]],1,[[2,6],0,[4,6,2,3],9]< and R:>10,[7],6<
Processing SUBBLOCK
LEFT : >>>6_,_[_[_0_]_,_[_6_,_7_]_,_[_4_,_1_0_,_8_,_4_,_1_]_]_,_1_,_[_[_2_,_6_]_,_0_,_[_4_,_6_,_2_,_3_]_,_9_]<<<
RIGHT: >>>1_0_,_[_7_]_,_6<<<
Warning 3: Out of inner loop 2...
[GetNextItem] Got ITEM: >>6<<
[...]
~~
LEFT : >>>[[[],[[8,6,1,6],[10]],[8,3,[2,1,2,6],[8,1,10,8],0]],[[0,[7,4]],5,7,[[10]]],[[[],8],[],8],[[6,[0,1],9,1,0],[],3,5,9]]<<<
RIGHT: >>>[[],[1],[7],[4,0,4,[]]]<<<
BLOCK >147< is >UNORDERED< 

~~
LEFT : >>>[[[7,3,6]],[8,[[],4,8,[6]],[1,4,[]],0,9],[]]<<<
RIGHT: >>>[[3,0,2,[]],[3,[],5,[]],[[[4,1,9,4],[5,10,3],9,[9,1]],[[10],7,[3]],[[],[2,5,0,10],8,5],[],4]]<<<
BLOCK >148< is >UNORDERED< 

~~
LEFT : >>>[[9,[9,[0,2]],10,[[0,7,9,4,2],2,[6,7,4,3],[7]],4],[],[[],10,4,5]]<<<
RIGHT: >>>[[[[9],[7],2,1,8],[[7,5,9],10],[],[]]]<<<
BLOCK >149< is >ORDERED< ...ADDING!

~~
LEFT : >>>[[1,3,[[7],3,[6,10],[7,2,10,6],6],5],[],[[],[[],2],1,[10,[],8,3,[9,7,9,8]]],[]]<<<
RIGHT: >>>[[[10,[10,7,9],[],[8,9]],7],[2,10,[0,9,[4,2,10,7]],6,9],[[1,2,3],0],[5,3,[2],2]]<<<
BLOCK >150< is >ORDERED< ...ADDING!

~~
TOTAL_SUM: [5694]
[verzulli@XPSGarr REV01_PERL]$ 
~~~

* NodeJS P1 - Correct (5366 is the right result)

~~~
[verzulli@XPSGarr REV02_NODEJS]$ npm start

> aoc_2022_day13@1.0.0 start
> babel-node ./solve_day13_REV02.js

30-12-2022 10:04:19.809 [AoC2022_DAY13] info: [MAIN] Starting PROCESSOR....
30-12-2022 10:04:19.811 [AoC2022_DAY13] info: [loadInput] Start loading...
30-12-2022 10:04:19.815 [AoC2022_DAY13] info: [rl - onClose] Fine dello stream!!
30-12-2022 10:04:19.815 [AoC2022_DAY13] info: [loadInput] read [150] records
30-12-2022 10:04:19.816 [AoC2022_DAY13] info: [process] Processing Block 1: >>>[[6,[[0],[6,7],[4,10,8,4,1]],1,[[2,6],0,[4,6,2,3],9]]]<<< - Right: >>>[[10,[7],6]]<<<
30-12-2022 10:04:19.816 [AoC2022_DAY13] info: [MAIN]: BLOCK [1] is: [true]
30-12-2022 10:04:19.816 [AoC2022_DAY13] info: [process] Processing Block 2: >>>[[[2,5,[0,2,6,4],[6],5],[1,[1,4,10]]],[7,[[],6,[9],[0,5,8,8,1],8]]]<<< - Right: >>>[[2,3],[8,10],[3],[[10,[1,3,8,0,7],2,6,4],[],[1,[9,1,4],[1,4,5,10],[]],[5,[],[7],2],[3,7,[10,3],7,[]]]]<<<
30-12-2022 10:04:19.816 [AoC2022_DAY13] info: [MAIN]: BLOCK [2] is: [false]
30-12-2022 10:04:19.816 [AoC2022_DAY13] info: [process] Processing Block 3: >>>[[[4,[0,8,2]],3,[[],[0,1,9,3,5],6],[]],[],[0,2,[[5,5,8],[8,6,5,10]],10]]<<< - Right: >>>[[[4,[9],[9,8,10,1,0]]],[7],[8,[0,[8,9,5,3],3,[8,4],4],0,[[]]]]<<<
30-12-2022 10:04:19.816 [AoC2022_DAY13] info: [MAIN]: BLOCK [3] is: [true]
30-12-2022 10:04:19.816 [AoC2022_DAY13] info: [process] Processing Block 4: >>>[[[],[8,5],9,10],[7,6,10,1],[[7,[3,5,0,1],2,0,[2,0,3,7]],3,10,2,[8]],[[5,[8,2,10],[5,8,2,8,7]],7,10,[],[[3,10,4,8,4],4,9,10,2]]]<<< - Right: >>>[[[[0,2,8,5,3],4],[[5,4,10],[],4,[3,1,4]],[],10,10],[[],[],2,10],[6,1,[6,[2,6,4,6,4],[6]]],[[],0],[]]<<<
30-12-2022 10:04:19.816 [AoC2022_DAY13] info: [MAIN]: BLOCK [4] is: [true]
30-12-2022 10:04:19.816 [AoC2022_DAY13] info: [process] Processing Block 5: >>>[[[],5],[2]]<<< - Right: >>>[[7,4,6,[4],[3,[7],[3,1,8,9,8]]],[7],[[]],[0,[[1,3,1,10],8,8,7]],[3,[8,[6,3,1]]]]<<<
30-12-2022 10:04:19.817 [AoC2022_DAY13] info: [MAIN]: BLOCK [5] is: [true]
30-12-2022 10:04:19.817 [AoC2022_DAY13] info: [process] Processing Block 6: >>>[[[],6],[[],10,4,[5,8],[5,5,[6,4,2,6,3],10]]]<<< - Right: >>>[[],[[6,5],8,5,[2,[2,2],[4,8,1],[]]]]<<<
30-12-2022 10:04:19.817 [AoC2022_DAY13] info: [MAIN]: BLOCK [6] is: [false]
[...]
30-12-2022 10:06:49.918 [AoC2022_DAY13] info: [MAIN]: BLOCK [147] is: [false]
30-12-2022 10:06:49.918 [AoC2022_DAY13] info: [process] Processing Block 148: >>>[[[7,3,6]],[8,[[],4,8,[6]],[1,4,[]],0,9],[]]<<< - Right: >>>[[3,0,2,[]],[3,[],5,[]],[[[4,1,9,4],[5,10,3],9,[9,1]],[[10],7,[3]],[[],[2,5,0,10],8,5],[],4]]<<<
30-12-2022 10:06:49.918 [AoC2022_DAY13] info: [MAIN]: BLOCK [148] is: [false]
30-12-2022 10:06:49.918 [AoC2022_DAY13] info: [process] Processing Block 149: >>>[[9,[9,[0,2]],10,[[0,7,9,4,2],2,[6,7,4,3],[7]],4],[],[[],10,4,5]]<<< - Right: >>>[[[[9],[7],2,1,8],[[7,5,9],10],[],[]]]<<<
30-12-2022 10:06:49.918 [AoC2022_DAY13] info: [MAIN]: BLOCK [149] is: [true]
30-12-2022 10:06:49.918 [AoC2022_DAY13] info: [process] Processing Block 150: >>>[[1,3,[[7],3,[6,10],[7,2,10,6],6],5],[],[[],[[],2],1,[10,[],8,3,[9,7,9,8]]],[]]<<< - Right: >>>[[[10,[10,7,9],[],[8,9]],7],[2,10,[0,9,[4,2,10,7]],6,9],[[1,2,3],0],[5,3,[2],2]]<<<
30-12-2022 10:06:49.918 [AoC2022_DAY13] info: [MAIN]: BLOCK [150] is: [true]
30-12-2022 10:06:49.918 [AoC2022_DAY13] info: SUM: [5366]
30-12-2022 10:06:49.919 [AoC2022_DAY13] info: [fileReadStream - onClose] Fine dello stream!!
[verzulli@XPSGarr REV02_NODEJS]$ 
~~~

* NodeJS P2 attempt - (failed solution)
    
   As you can see, it's even unable to sort TEST input...

   After struggling a bit... a decided to stop working on it :-()

~~~
[verzulli@XPSGarr REV02_NODEJS]$ npm run p2

> aoc_2022_day13@1.0.0 p2
> babel-node ./solve_day13_P2.js

30-12-2022 10:08:16.609 [AoC2022_DAY13] info: [MAIN] Starting PROCESSOR....
30-12-2022 10:08:16.611 [AoC2022_DAY13] info: [loadInput] Start loading...
30-12-2022 10:08:16.618 [AoC2022_DAY13] info: [rl - onClose] Fine dello stream!!
30-12-2022 10:08:16.618 [AoC2022_DAY13] info: [loadInput] read [18] records
30-12-2022 10:08:16.619 [AoC2022_DAY13] info: [process] Block [1] >>>[1,1,3,1,1]<<<
30-12-2022 10:08:16.619 [AoC2022_DAY13] info: [process] Block [2] >>>[1,1,5,1,1]<<<
30-12-2022 10:08:16.619 [AoC2022_DAY13] info: [process] Block [3] >>>[[1],[2,3,4]]<<<
30-12-2022 10:08:16.619 [AoC2022_DAY13] info: [process] Block [4] >>>[[1],4]<<<
30-12-2022 10:08:16.619 [AoC2022_DAY13] info: [process] Block [5] >>>[9]<<<
30-12-2022 10:08:16.619 [AoC2022_DAY13] info: [process] Block [6] >>>[[8,7,6]]<<<
30-12-2022 10:08:16.619 [AoC2022_DAY13] info: [process] Block [7] >>>[[4,4],4,4]<<<
30-12-2022 10:08:16.619 [AoC2022_DAY13] info: [process] Block [8] >>>[[4,4],4,4,4]<<<
30-12-2022 10:08:16.619 [AoC2022_DAY13] info: [process] Block [9] >>>[7,7,7,7]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: [process] Block [10] >>>[7,7,7]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: [process] Block [11] >>>[]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: [process] Block [12] >>>[3]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: [process] Block [13] >>>[[[]]]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: [process] Block [14] >>>[[]]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: [process] Block [15] >>>[1,[2,[3,[4,[5,6,7]]]],8,9]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: [process] Block [16] >>>[1,[2,[3,[4,[5,6,0]]]],8,9]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: [process] Block [17] >>>[[2]]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: [process] Block [18] >>>[[6]]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: Need to NOT swap [0/1] => >>>[1,1,3,1,1]<<< >>>[1,1,5,1,1]<<<
30-12-2022 10:08:16.620 [AoC2022_DAY13] info: Need to NOT swap [0/2] => >>>[1,1,3,1,1]<<< >>>[[1],[2,3,4]]<<<
[...]
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: Need to NOT swap [15/16] => >>>[1,[2,[3,[4,[5,6,0]]]],8,9]<<< >>>[[2]]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: Need to NOT swap [15/17] => >>>[1,[2,[3,[4,[5,6,0]]]],8,9]<<< >>>[[6]]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: Need to NOT swap [16/17] => >>>[[2]]<<< >>>[[6]]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [11] >>>[]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [1] >>>[1,1,3,1,1]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [4] >>>[[1],4]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [3] >>>[[1],[2,3,4]]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [2] >>>[1,1,5,1,1]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [6] >>>[[8,7,6]]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [7] >>>[[4,4],4,4]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [12] >>>[3]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [8] >>>[[4,4],4,4,4]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [13] >>>[[[]]]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [9] >>>[7,7,7,7]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [5] >>>[9]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [14] >>>[[]]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [10] >>>[7,7,7]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [18] >>>[[6]]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [15] >>>[1,[2,[3,[4,[5,6,7]]]],8,9]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [16] >>>[1,[2,[3,[4,[5,6,0]]]],8,9]<<<
30-12-2022 10:08:16.627 [AoC2022_DAY13] info: [process] Block [17] >>>[[2]]<<<
30-12-2022 10:08:16.628 [AoC2022_DAY13] info: [fileReadStream - onClose] Fine dello stream!!
[verzulli@XPSGarr REV02_NODEJS]$  


~~~