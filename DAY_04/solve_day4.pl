#!/usr/bin/perl

use strict;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------
  
# Let's open the file for reading
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Let's initialize the temporary score SUM
my $result_total = 0;
my $total_overlap = 0;

# Let's read the file, line by line
while (my $line = <FH>) {

  # let's get rid of the terminating newline
  chomp($line);

  # let's do some magic, here :-)
  # (aka: extract the 4 values of the two ranges)
  if ($line =~ /^(\d+)\-(\d+),(\d+)\-(\d+)$/) {
    my ($l1, $l2, $r1, $r2) =  ($1, $2, $3, $4);

    # let's check if left range is included in right one
    my $left_in_right = 1;
    foreach my $idx ($l1..$l2) {
      if (($idx < $r1) or ($idx > $r2)) {
        $left_in_right = 0;
        last;
      }
    };

    # let's check if right range is included in left one
    my $right_in_left = 1;
    foreach my $idx ($r1..$r2) {
      if (($idx < $l1) or ($idx > $l2)) {
        $right_in_left = 0;
        last;
      }
    };

    # if one of the above, increase the total
    if ($left_in_right or $right_in_left) {
      printf("IN: [%d]-[%d];[%d]-[%d]\n", $l1, $l2, $r1, $r2);
      $result_total++;
    }

    if (
      ($r1 >= $l1 and $r1 <= $l2) or
      ($r2 >= $l1 and $r2 <= $l2) or
      ($l1 >= $r1 and $l1 <= $r2) or
      ($l2 >= $r1 and $l2 <= $r2)
    ) {
      printf("OV: [%d]-[%d];[%d]-[%d]\n", $l1, $l2, $r1, $r2);
      $total_overlap++;
    }

  } else {
    print "CAOS!\n";
    exit;
  }
}
print "Number fully overlapping: [" . $result_total . "]\n";
print "Number overlapping: [" . $total_overlap . "]\n";
exit;
