TL/DR;
======
Being given a list of couples of ranges (in the form [a-b, c-d]), count the number of couples where one range is completely included in the other

Original text
=============

    [...]
    For example, consider the following list of section assignment pairs:

    2-4,6-8
    2-3,4-5
    5-7,7-9
    2-8,3-7
    6-6,4-6
    2-6,4-8

    For the first few pairs, this list means:

        Within the first pair of Elves, the first Elf was assigned sections 2-4 (sections 2, 3, and 4), while the second Elf was assigned sections 6-8 (sections 6, 7, 8).
        The Elves in the second pair were each assigned two sections.
        The Elves in the third pair were each assigned three sections: one got sections 5, 6, and 7, while the other also got 7, plus 8 and 9.

    This example list uses single-digit section IDs to make it easier to draw; your actual list might contain larger numbers. Visually, these pairs of section assignments look like this:

    .234.....  2-4
    .....678.  6-8

    .23......  2-3
    ...45....  4-5

    ....567..  5-7
    ......789  7-9

    .2345678.  2-8
    ..34567..  3-7

    .....6...  6-6
    ...456...  4-6

    .23456...  2-6
    ...45678.  4-8

    Some of the pairs have noticed that one of their assignments fully contains the other. For example, 2-8 fully contains 3-7, and 6-6 is fully contained by 4-6. In pairs where one assignment fully contains the other, one Elf in the pair would be exclusively cleaning sections their partner will already be cleaning, so these seem like the most in need of reconsideration. In this example, there are 2 such pairs.

    In how many assignment pairs does one range fully contain the other?

My notes
========
I'm going to implement the solution in [PERL](https://www.perl.org), as it's perfect to read input from files and to work properly with strings, extracting range borders

* My input data are in the "input.txt"
* My implemented solution is "solve_day4.pl"

Solving approach
----------------

* extract range borders (`$l1`:`$l2`-`$r1`:`$r1`) with a magic regexp like... `/^(\d+)\-(\d+),(\d+)\-(\d+)$/`
* check if LEFT values are fully included in RIGHT values *OR*...
* ...if RIGHT values are fully included in LEFT ones
* if one of the above, increase the result totals

Implementation
==============

Please refer to:
* [`solve_day4.pl`](./solve_day4.pl) - for both Part 1 and Part 2

Results
=======

    [verzulli@XPSGarr DAY_04]$ perl solve_day4.pl
    [14]-[50];[14]-[50]
    [43]-[44];[43]-[87]
    [22]-[92];[21]-[92]
    [4]-[80];[3]-[80]
    [10]-[67];[34]-[67]
    [...]
    [56]-[70];[55]-[93]
    [51]-[95];[51]-[51]
    [45]-[73];[46]-[73]
    [17]-[36];[17]-[37]
    [98]-[98];[17]-[99]
    Number fully overlapping: [540]

Results for Part 2
==================

    [verzulli@XPSGarr DAY_04]$ perl solve_day4.pl
    IN: [14]-[50];[14]-[50]
    OV: [14]-[50];[14]-[50]
    IN: [43]-[44];[43]-[87]
    OV: [43]-[44];[43]-[87]
    OV: [55]-[99];[51]-[96]
    OV: [67]-[68];[68]-[91]
    [...]
    OV: [45]-[73];[46]-[73]
    OV: [35]-[38];[36]-[39]
    IN: [17]-[36];[17]-[37]
    OV: [17]-[36];[17]-[37]
    IN: [98]-[98];[17]-[99]
    OV: [98]-[98];[17]-[99]
    Number fully overlapping: [540]
    Number overlapping: [872]
