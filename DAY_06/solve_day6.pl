#!/usr/bin/perl

use strict;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

my %mat = (
  1 => ['S', 'T', 'H', 'F', 'W', 'R'],
  2 => ['S','G','D','Q','W'],
  3 => ['B','T','W'],
  4 => ['D','R','W','T','N','Q','Z','J'],
  5 => ['F','B','H','G','L','V','T','Z'],
  6 => ['L','P','T','C','V','B','S','G'],
  7 => ['Z','B','R','T','W','G','P'],
  8 => ['N','G','M','T','C','J','R'],
  9 => ['L','G','B','W']
);

# Let's open the file for reading
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

my $whole_text = <FH>;

chomp ($whole_text);

my $size = length($whole_text);

my $mlen = 14;

foreach my $idx (0..$size - $mlen) {
  my $part = substr($whole_text, $idx, $mlen);
  my %charmap = ();

  foreach my $j (0..$mlen-1) {
    $charmap{substr($part,$j,1)}++;
  }
  if (scalar keys(%charmap) == $mlen) {
    printf("%s size: %d - @%d\n",$part,scalar keys(%charmap), $idx+$mlen);
    last;
  }
}
