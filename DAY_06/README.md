TL/DR;
======
Being given a very long string, find the first group of 4 chars, all different from each other. Report the position of the 4th char.

Original text
=============

    [...]
    your subroutine needs to identify the first position where the four most recently received characters were all different. Specifically, it needs to report the number of characters from the beginning of the buffer to the end of the first such four-character marker.

    For example, suppose you receive the following datastream buffer:

    mjqjpqmgbljsphdztnvjfqwrcgsmlb

    After the first three characters (mjq) have been received, there haven't been enough characters received yet to find the marker. The first time a marker could occur is after the fourth character is received, making the most recent four characters mjqj. Because j is repeated, this isn't a marker.

    The first time a marker appears is after the seventh character arrives. Once it does, the last four characters received are jpqm, which are all different. In this case, your subroutine should report the value 7, because the first start-of-packet marker is complete after 7 characters have been processed.

My notes
========
I'm going to implement the solution in [PERL](https://www.perl.org).

* My input data are in the "input.txt"
* My implemented solution is "solve_day6.pl"

Solving approach
----------------

* loop over the main string, in 4-chars sliding "chunk";
* to check if all chars are different, I'm going to use each char as an HASH key, and counting the number of the keys at the end;
* if "4", just print the index of the loop + 4

To solve P2, I've just parametrized the "4" and configured it as "14"

Implementation
==============

Please:
* refer to [`solve_day6.pl`](./solve_day6.pl)

Results
=======
    [verzulli@XPSGarr DAY_06]$ perl solve_day6.pl 
    pwmb size: 4 - @1109

Results for Part 2
==================
    [verzulli@XPSGarr DAY_06]$ perl solve_day6.pl 
    cvjsqlftnpdhwm size: 14 - @3965
