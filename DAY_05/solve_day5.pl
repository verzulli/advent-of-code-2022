#!/usr/bin/perl

use strict;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

my %mat = (
  1 => ['S', 'T', 'H', 'F', 'W', 'R'],
  2 => ['S','G','D','Q','W'],
  3 => ['B','T','W'],
  4 => ['D','R','W','T','N','Q','Z','J'],
  5 => ['F','B','H','G','L','V','T','Z'],
  6 => ['L','P','T','C','V','B','S','G'],
  7 => ['Z','B','R','T','W','G','P'],
  8 => ['N','G','M','T','C','J','R'],
  9 => ['L','G','B','W']
);

# Let's open the file for reading
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Let's read the file, line by line
while (my $line = <FH>) {

  # skip comments
  next if ($line =~ /^#/);

  # let's get rid of the terminating newline
  chomp($line);

  # let's do some magic, here :-)
  # (aka: extract the 3 values we need)
  if ($line =~ /^move\s(\d+)\sfrom\s(\d+)\sto\s(\d+)$/) {
    my ($numMoves, $from, $to) =  ($1, $2, $3);

    # Let's fetch the slice to move
    my $offset = 0 - $numMoves;
    my @payload = splice(@{$mat{$from}}, $offset);

    # push the value to the $to array
    push(@{$mat{$to}}, @payload);
  } else {
    print "CAOS!\n";
    exit;
  }
}

# dump the resulting matrix
foreach my $idx (sort keys %mat) {
  printf("i: [%d] => ", $idx);
  foreach my $j (@{$mat{$idx}}) {
    print "[".$j."]";
  }
  print "\n";
}

# dump the results
foreach my $idx (sort keys %mat) {
  printf("%s", pop(@{$mat{$idx}}));
}
print "\n";


