TL/DR;
======
Being given a list of stacks, you have to move the head of the stacks based on rules defined in a file.

In short: you have to POP and PUSH over arrays...

Original text
=============

    [...]
    For example:

        [D]    
    [N] [C]    
    [Z] [M] [P]
    1   2   3 

    move 1 from 2 to 1
    move 3 from 1 to 3
    move 2 from 2 to 1
    move 1 from 1 to 2

    In this example, there are three stacks of crates. Stack 1 contains two crates: crate Z is on the bottom, and crate N is on top. Stack 2 contains three crates; from bottom to top, they are crates M, C, and D. Finally, stack 3 contains a single crate, P.

    Then, the rearrangement procedure is given. In each step of the procedure, a quantity of crates is moved from one stack to a different stack. In the first step of the above rearrangement procedure, one crate is moved from stack 2 to stack 1, resulting in this configuration:

    [D]        
    [N] [C]    
    [Z] [M] [P]
    1   2   3 

    In the second step, three crates are moved from stack 1 to stack 3. Crates are moved one at a time, so the first crate to be moved (D) ends up below the second and third crates:

            [Z]
            [N]
        [C] [D]
        [M] [P]
    1   2   3

    Then, both crates are moved from stack 2 to stack 1. Again, because crates are moved one at a time, crate C ends up below crate M:

            [Z]
            [N]
    [M]     [D]
    [C]     [P]
    1   2   3

    Finally, one crate is moved from stack 1 to stack 2:

            [Z]
            [N]
            [D]
    [C] [M] [P]
    1   2   3

    The Elves just need to know which crate will end up on top of each stack; in this example, the top crates are C in stack 1, M in stack 2, and Z in stack 3, so you should combine these together and give the Elves the message CMZ.

    After the rearrangement procedure completes, what crate ends up on top of each stack?

My notes
========
I'm going to implement the solution in [PERL](https://www.perl.org), as it's perfect to read input from files and extract proper values (the number of moves, the FROM stack and the TO stack).

I'm also defining the starting matrix, directly in the code, as an HASH of ARRAYS, where the HASH key is the stack number and the ARRAYS are the related letters.

* My input data are in the "input.txt"
* My implemented solution is "solve_day5.pl"

Solving approach
----------------

* define the starting matrix (one shot; in code);
* comment related rows in the file
* process the file, extracting  `$numMoves`, `$from`, and `$to` values
* apply the moves

As for P2, simply replace the for loop with a splice.

Implementation
==============

Please:
* refer to [`solve_day5.pl`](./solve_day5.pl) for Part2 solution;
* refer to previous commit (or `git diff`) for Part1 solution.

Results
=======

    [verzulli@XPSGarr DAY_05]$ perl solve_day5.pl 
    i: [1] => [Z]
    i: [2] => [G][T][S][W][T][D][Z][R][M][J][R]
    i: [3] => [V][W][T][T][L]
    i: [4] => [G][P][L][Q][J]
    i: [5] => [Z][B][P][G][L][C][B][W][N][G]
    i: [6] => [H][S]
    i: [7] => [D][W][H][B][V][W][G][R][T][B][T][F][Q][W][F][S][N][C]
    i: [8] => [T]
    i: [9] => [B][G][R]
    ZRLJGSCTR
    [verzulli@XPSGarr DAY_05]$

Results for Part 2
==================

    [verzulli@XPSGarr DAY_05]$ perl solve_day5.pl 
    i: [1] => [P]
    i: [2] => [S][B][H][W][G][L][H][Z][B][R][R]
    i: [3] => [Z][M][C][J][T]
    i: [4] => [L][W][G][D][T]
    i: [5] => [T][J][N][B][T][S][W][W][N][G]
    i: [6] => [G][R]
    i: [7] => [V][T][W][L][D][R][Z][Q][B][T][F][Q][T][C][G][S][W][F]
    i: [8] => [P]
    i: [9] => [V][G][B]
    PRTTGRFPB
    [verzulli@XPSGarr DAY_05]$ 
