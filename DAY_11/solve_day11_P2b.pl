#!/usr/bin/perl

use strict;
use Data::Dumper;
use Math::BigInt;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

sub PlayMonkey($);

my %monkeys = ( );
my $rounds_to_play = 457;

# Let's open the file for reading...
open(FH, "./input_test.txt") || die ("Unable to open input.txt: $!");

# Let's load the data and populate the in-memory data structure (%monkeys)...
while (my $line = <FH>) {

  # remove ending newline
  chomp($line);

  # remove empty lines
  next if ($line =~ /^$/);

  # this is current monkey data-structure
  my %tmp_monkey;

  # Let's add a counter to keep track of the number of items processed...
  $tmp_monkey{'num_item_processed'}=0;

  # It's a new monkey? (should be YES!)
  if ($line =~ /^Monkey\s(\d+):$/) {

    # current monkey ID
    my $this_monkey = $1;
    # debug
    # print "Monkey: [".$this_monkey."]\n";

    # Process: Starting items: .., ..
    my $b1 = <FH>; chomp($b1);  
    if ($b1 =~ /Starting items:\s([\d\,\s]+)/) {
      my $start_str = $1;
      $start_str =~ s/\s//g;
      my @items_short = split(/,/,$start_str);
      my @items;
      foreach my $s (@items_short) {
        my $long = Math::BigInt->new($s);
        push (@items,$long);
      }

      # convert items to LONG
      $tmp_monkey{'items'} = \@items;

      # debug
      # print "Starting: (".join('|',@items).")\n";
    } else {
      print "Error B1\n";
    }

    # Process: Operation:
    my $b2 = <FH>; chomp($b2);
    if ($b2 =~ /Operation:\s(.*)$/) {
      my $operation_str = $1;
      $tmp_monkey{'operation'} = $operation_str;

      # debug
      # print "Operation: [".$operation_str."]\n";
    } else {
      print "Error B2\n";
    }

    # Process: Test:
    my $b3 = <FH>; chomp ($b3);
    if ($b3 =~ /Test:\sdivisible by (\d+)$/) {

      my $div_by = $1;
      $tmp_monkey{'div_by'} = $div_by;

      # debug
      # print "Divisible by: [$1]\n";

      my $l1 = <FH>;
      if ($l1 =~ /If true: throw to monkey (\d+)$/) {
        my $go_if_true = $1;
        $tmp_monkey{'go_if_true'} = $go_if_true;

        # debug
        # print "True: [". $go_if_true ."]\n";
      } else {
        print "Error true\n";
      }

      my $l2 = <FH>;
      if ($l2 =~ /If false: throw to monkey (\d+)$/) {
        my $go_if_false = $1;
        $tmp_monkey{'go_if_false'} = $go_if_false;
        # debug
        # print "False: [". $go_if_false ."]\n";
      } else {
        print "Error false\n";
      }      
    } else {
      print "Error B3\n";
    }

    # let's add current monkey to the main HASH of all monkeys
    $monkeys{$this_monkey}=\%tmp_monkey;

  } else {
    print "Error M1 [".$line."]\n";
  }
}

#######################################
# Let's play...
foreach my $round (1..$rounds_to_play) {
  print "\n\nPlaying round [" . $round . "]\n";

  # let's loop over all monkeys (ordered)
  foreach my $m (sort keys %monkeys) {
    PlayMonkey($m);
  }
}

#######################################
# Let's get the results

# let's sort by num_item_processed, reversed;
my @list = sort { $monkeys{$b}{'num_item_processed'} <=> $monkeys{$a}{'num_item_processed'} } keys %monkeys;
foreach my $r (sort keys %monkeys) {
  printf("monkey [%s] - num processed: [%d] - ",$r, $monkeys{$r}{'num_item_processed'});
  foreach my $ln (@{$monkeys{$r}{'items'}}) {
    print $ln->bdstr() . "; ";
  };
  print "\n";
  # printf("monkey [%s] - num processed: [%d]\n",$r, $monkeys{$r}{'num_item_processed'});
}
my $first = $monkeys{$list[0]}{'num_item_processed'};
my $second = $monkeys{$list[1]}{'num_item_processed'};
my $product = $first * $second;
printf("Result is [%d]x[%d]=[%d]\n",$first, $second, $product);
# print Dumper(\%monkeys);
# print "\n";

exit;

sub PlayMonkey($) {
  my ($id) = @_;

  my %m = %{$monkeys{$id}};

  # Loop over all items
  while (my $i = shift @{$m{'items'}}) {

    # printf("Playing Monkey [%2d] - item [%s]\n", $id, $i->bstr());

    # Let's increase the counter
    $monkeys{$id}{'num_item_processed'}++;

    my $wl_tmp = $i->copy();
    my $op_str = $m{'operation'};

    if ( $op_str =~ /^new\s=\sold\s(.)\s(\d+)$/ ) {
      # second operand is a number...
      my ($op_op, $op_value) = ($1, $2);
      if ($op_op eq '+') {
        $wl_tmp->badd($op_value);
      } elsif ($op_op eq '-') {
        $wl_tmp->bsub($op_value);
      } elsif ($op_op eq '*') {
        $wl_tmp->bmul($op_value);
      } else {
        print "Bad operand A!\n";
      }
    } elsif ( $op_str =~ /^new\s=\sold\s(.)\sold$/ ) {
      # second operand is a "old"...
      my $op_op = $1;
      if ($op_op eq '+') {
        $wl_tmp->bmul(2);
      } elsif ($op_op eq '-') {
        $wl_tmp = Math::BigInt->bzero();
      } elsif ($op_op eq '*') {
        $wl_tmp->bpow(2);
      } else {
        print "Bad operand B!\n";
      }
    } else {
      print "Bad operation - C\n";
    }

    # divide by three and round, to obtain NEW WL
    # my $wl = sprintf("%d", $wl_tmp / 3); <= P1
    my $wl_clone = $wl_tmp->copy();

    my $div_factor = $m{'div_by'};
    my $target_monkey;

    # check divisibility...
    if ($wl_clone->bmod($div_factor) == 0) {
      $target_monkey = $m{'go_if_true'};
    } else {
      $target_monkey = $m{'go_if_false'};
    }
    # push new WL to target monkey
    push (@{$monkeys{$target_monkey}{'items'}}, $wl_tmp);
  }
}
