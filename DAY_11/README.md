TL/DR;
======
Given a set of rule (textual) related to multiple players, you have to follow such rules in a "game" involving players.

The game is based on single "round" where each player play all of its `items`, passing such cards to other players.

After 20 rounds, you have to check the most active players and make some math on them.

Hint: this is getting *DIFFICULT*

Original text
=============

Better to refer to original text, [here](https://adventofcode.com/2022/day/11).


Solving approach
----------------
* I'll read the file and populate a `%monkeys` datastructure to hold the whole figures;
* I'll heavily use RegExps to check inputs and extract values;
* I'll apply the rules keeping track of changes in the `%monkeys` structure;
* while playing, I keep track of the number of playing in the `num_item_processed` property;
* at the end, I create a list of monkeys, based on the num_item_processed and extract the first two, to multiply them and get final result.

As for P2, I initially tryed to solve the problem of "big numbers" by adopting [Math::BigInt](https://perldoc.perl.org/Math::BigInt) but... unfortunatly this was still unable to scale over ~500 iterations. You can check results by yourself in `solve_day11_P2b.pl`. Anyway, it has been interesting, as I got in touch with `2^64` that I still didn't know :-) ....and ad I saw some **VERY** big integers (with hundreds of ciphers!).

So, after initial failure with `Math::BigInt` I simply noted that... I could divide each WL by the product of all the dividends and continuing the elaboration with related modulus (need to think a little but.... you can easily see that `( [a x b x c] x n + m ) / (a x b x c) == n + m`. So let divide the WL by `(a x b x c)` and keep working with `n` (the modulus).

Implementation
==============

Please:
* refer to [`solve_day11.pl`](./solve_day11.pl)
* refer to [`solve_day11_P2.pl`](./solve_day11_P2.pl)
* refer to [`solve_day11_P2b.pl`](./solve_day11_P2b.pl) - this won't solve P2, but show the use (and problems) of [Math::BigInt](https://perldoc.perl.org/Math::BigInt)

Results
=======

    Playing round [1]
    Playing Monkey [ 0] - item [80]
    False: Next is: [3]
    Done!
    Playing Monkey [ 1] - item [75]
    False: Next is: [6]
    Playing Monkey [ 1] - item [83]
    False: Next is: [6]
    Playing Monkey [ 1] - item [74]
    False: Next is: [6]
    Done!
    Playing Monkey [ 2] - item [86]
    True: Next is: [7]
    Playing Monkey [ 2] - item [67]
    [...]
    Playing Monkey [ 5] - item [3]
    False: Next is: [2]
    Playing Monkey [ 5] - item [3]
    False: Next is: [2]
    Done!
    Playing Monkey [ 6] - item [876044]
    True: Next is: [2]
    Done!
    Done!
    monkey [0] - num processed: [329] - items: [2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2]
    monkey [2] - num processed: [305] - items: [2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;2;255817696645]
    monkey [5] - num processed: [300] - items: []
    monkey [3] - num processed: [296] - items: []
    monkey [7] - num processed: [79] - items: []
    monkey [6] - num processed: [58] - items: []
    monkey [1] - num processed: [56] - items: []
    monkey [4] - num processed: [51] - items: []
    Result is [329]x[305]=[100345]

Results for P2
==============

    [verzulli@XPSGarr DAY_11]$ perl solve_day11_P2.pl  | head -n 20
    monkey [0] - div by [2]
    monkey [1] - div by [7]
    monkey [2] - div by [3]
    monkey [3] - div by [17]
    monkey [4] - div by [11]
    monkey [5] - div by [19]
    monkey [6] - div by [5]
    monkey [7] - div by [13]
    Prod Factor = [9699690]
    Playing round [1]
    Playing Monkey [ 0] - item [80]
    True: Next is: [4]
    Done!
    Playing Monkey [ 1] - item [75]
    False: Next is: [6]
    Playing Monkey [ 1] - item [83]
    False: Next is: [6]
    Playing Monkey [ 1] - item [74
    [...]
    Playing Monkey [ 7] - item [5558241]
    False: Next is: [0]
    Playing Monkey [ 7] - item [4235721]
    False: Next is: [0]
    Playing Monkey [ 7] - item [7330341]
    False: Next is: [0]
    Playing Monkey [ 7] - item [8757315]
    False: Next is: [0]
    Playing Monkey [ 7] - item [4293886]
    False: Next is: [0]
    Done!
    monkey [0] - num processed: [174439] - items: [402521;4429381;1480871;5535211;8313271;4415621;3829411;4711201;6550291;5872471;5558247;4235727;7330347;8757321;4293892]
    monkey [1] - num processed: [27450] - items: [3173543;8280234;5520484]
    monkey [2] - num processed: [155408] - items: [5292776;3244446;2264376;6853676;5535206;4415616;6152156;9679316;6152156;3829406;3829406;3829406;5150096;6474836;8855056;8855056;2682526;8570806]
    monkey [3] - num processed: [163595] - items: []
    monkey [4] - num processed: [13535] - items: []
    monkey [5] - num processed: [152578] - items: []
    monkey [6] - num processed: [32741] - items: []
    monkey [7] - num processed: [49410] - items: []
    Result is [174439]x[163595]=[28537348205]

