#!/usr/bin/perl

# NON è 442
# NON è 138

use strict;
use Data::Dumper;
use Term::Cap;
use Graph::Dijkstra;

$|=1;
$Data::Dumper::Maxdepth = 2;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

sub GetKey($$);
sub DumpMap();
sub Work($$$$);

my $map; # array of array_refs, for global MAP;
my $working_queue; # array of HASH refs

my @path; # array of Y|X, Y|X, Y|X.... of current path

# Let's open the file for reading...
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Let's load the data and populate the in-memory data structure (%monkeys)...
while (my $line = <FH>) {

  # remove ending newline
  chomp($line);

  #split the line and populate the array
  my @row_list = split(//, $line); 
  push(@{$map}, \@row_list);

}

my $num_map_rows = scalar @{$map};
my $num_map_cols = scalar @{$map->[0]};

# Start position - "S"
my %S = (
  'x' => -1,
  'y' => -1,
);

my %E = (
  'x' => -1,
  'y' => -1,
);

my $end_y = -1;

for (my $r = 0; $r < $num_map_rows; $r++) {
  my $cols = $map->[$r];
  for (my $c = 0; $c < $num_map_cols; $c++) {
    my $cur_elevation = $map->[$r][$c];
    if ($cur_elevation eq 'S') {
      $S{'X'} = $c;
      $S{'Y'} = $r;
      $map->[$r][$c] = {
        'elevation' => ord('a')-96,
        'visited' => 0,
        'row' => $r,
        'column' => $c,
        'parent' => {}
      };
    } elsif ($cur_elevation eq 'E') {
      $E{'X'} = $c;
      $E{'Y'} = $r;
      $map->[$r][$c] = {
        'elevation' => ord('z')-96,
        'visited' => 0,
        'row' => $r,
        'column' => $c,
        'parent' => {}
      };
    } else {
      $map->[$r][$c] = {
        'elevation' => ord($map->[$r][$c])-96,
        'visited' => 0,
        'row' => $r,
        'column' => $c,
        'parent' => {}
      };
    }
  }
}

printf("Coordinates are [<ROW/Y>;<COLUMN/X>]\n");
printf("Map size: [%d]x[%d]\n", $num_map_rows, $num_map_cols);
printf("From S: [%d;%d] To E: [%d;%d]\n", $S{'Y'}, $S{'X'}, $E{'Y'}, $E{'X'});

# Let's start playing (...easy)
my $graph = Graph::Dijkstra->new();  #create the graph object with default attribute values
$graph->graph({'edgedefault'=>'directed'});

# Create all nodes
for (my $r = 0; $r < $num_map_rows; $r++) {
  for (my $c = 0; $c < $num_map_cols; $c++) {
    
    my $key = GetKey($r, $c);
    # create node
    $graph->node( {'id'=>$key, 'label'=>$key } );   
  }
}

# DumpMap();

# Create all edges
for (my $r = 0; $r < $num_map_rows; $r++) {
  for (my $c = 0; $c < $num_map_cols; $c++) {

    my $this_node_ref = $map->[$r][$c];
    my $this_node_key = GetKey($r, $c);
    
    if ($c < $num_map_cols - 1) {
      # ADD RIGHT
      Work($this_node_ref, $r, $c+1, ' => ');
    }
    if ($c > 0) {
      # ADD LEFT
      Work($this_node_ref, $r, $c-1, ' <= ');
    }
    if ($r < $num_map_rows - 1) {
      # ADD DOWN
      Work($this_node_ref, $r+1, $c, '  v ');
    }
    if ($r > 0) {
      # ADD UP
      Work($this_node_ref, $r-1, $c, '  ^ ');
    }
  };
}

my $source_node_key = GetKey($S{'Y'}, $S{'X'});
my $destination_node_key = GetKey($E{'Y'}, $E{'X'});

my $iteration = 1;
#shortest path between two nodes (from origin to destination)

my $solution = {'originID' => $source_node_key, 'destinationID' => $destination_node_key };
if (my $pathCost = $graph->shortestPath( $solution ) ) {
  printf("Shortest path len: [%d]/[%d]\n", $solution->{'weight'}, $pathCost);
#   foreach my $e (@{$solution->{'edges'}}) {
#     printf("%3d: [%s] => [%s]\n", $iteration++, $e->{'sourceID'}, $e->{'targetID'});
#   }
} else {
  print "No path found!\n";
}

exit;

sub Work($$$$) {
  my ($cur_node_ref, $r, $c, $label) = @_;
  
  my $src_key = GetKey($cur_node_ref->{'row'}, $cur_node_ref->{'column'});
  my $next_node_key = GetKey($r, $c);
  my $next_node_ref = $map->[$r][$c];
  my $this_level = $cur_node_ref->{'elevation'};
  my $next_level = $next_node_ref->{'elevation'};
  my $difference = $next_level - $this_level;
  if (($next_level == $this_level + 1) || ($this_level >= $next_level)) {
    # debug
    # printf("Add [%s] edge: [%s]/%d->[%s]/%d\n",$label, $src_key,$this_level,$next_node_key,$next_level);
    $graph->edge( {'sourceID'=>$src_key, 'targetID'=>$next_node_key, weight=>1} );
  } else {
    #debug
    # printf("MIS [%s] edge: [%3d] [%s]/%d->[%s]/%d\n",$label, $difference, $src_key,$this_level,$next_node_key,$next_level);
  }
};

sub GetKey($$) {
  my ($r, $c) = @_;

  my $key = sprintf("%d;%d",$r, $c);
  return $key;
}

sub DumpMap() {
  for (my $r = 0; $r < $num_map_rows; $r++) {
    printf("Row [%3d]: ", $r);
    my $cols = $map->[$r];
    for (my $c = 0; $c < $num_map_cols; $c++) {
      my $cur_elevation = $map->[$r][$c]{'elevation'};
      printf(" %2d",$cur_elevation);
    };
    print "\n";
  }
}