TL/DR;
======
We're given a graph, as a matrix of nodes where edges exist only between consecutive letters (sort of...): a=>b, d=>e but not e=>g. Edges exists also backward, regardless of distance (g=>e is a valid edge).
For P1 you have to find the shortest path between S and E, where the only S is an "a" and the only E is a "z"
For P2 you have to find the shortest path between any "a" (including "S") and E.

Original text
=============

Better to refer to original text, [here](https://adventofcode.com/2022/day/12).


Solving approach
----------------
* I converted a-z in related ASCII code, zero based (a=0);
* I built a Dijkstra algorithm to build and traverse the graph
* I got final results.

I opted to build a `$map` of the whole field, detailing every cells (indexed by *row* and *column*) and populating each cells with relevant data (*elevation*, *distance*, ...).

I understood *LOT* of things, along the journey:

* I reminded myself that graph need to be traverse by *width" (and not "by depth", with recursion), hence the `$working_queue`;
* I spent **two whole days** figuring out which was the BUG that led me to the wrong 442! At the end, after implementing [a completely new application](solve_day12_with_library.pl) with a ready-made [Graph::Dijstra library](https://metacpan.org/pod/Graph::Dijkstra)... and see it also giving out a 442, I finaly discovered that I mistakenly assigned "E" a `z+1` value... while it should have been `z` :-(

As for P2, things were easy: just adapt the previous calculation, looping over all the `a`;

Implementation
==============

Please:
* refer to [`solve_day12.pl`](./solve_day12.pl) for my solution, without relying on any external libs;
* refer to [`solve_day12_with_library.pl`](./solve_day12_with_library.pl) for the implementation I wrote, using an external library, as my attempts were failing (...but not due to bad-implementation! But 'cause I mistakenly assigned E a wrong value!)
* refer to [`solve_day12_P2.pl`](./solve_day12_P2b.pl) for the solution to P2

Results
=======

    [verzulli@XPSGarr DAY_12]$ perl solve_day12.pl
    Coordinates are [<ROW/Y>;<COLUMN/X>]
    Map size: [41]x[161]
    From S: [20;0] To E: [20;138]
    Working with: [20;0]/1 - starting distance: [0] - visited: [0] - CURRENT DISTANCE: [0]
    WARNING: this has been requeued!
    Check target:[20;1] with declared distance [99999] and visited status: [0]
    Check target:[21;0] with declared distance [99999] and visited status: [0]
    Check target:[19;0] with declared distance [99999] and visited status: [0]
    Working with: [20;0]/1 - final distance: [0] - visited: [0] - CURRENT DISTANCE: [0]
    Currently visited: [20;0-D:0] 
    Queue: [20;1-D:99999][21;0-D:99999][19;0-D:99999]
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Working with: [20;1]/2 - starting distance: [99999] - visited: [0] - CURRENT DISTANCE: [1]
    Check target:[20;2] with declared distance [99999] and visited status: [0]
    Check target:[20;0] with declared distance [0] and visited status: [1]
    Current Node:[20;1]/distance:[1] - Target Node:[20;0]/distance:[0] - Our path distance value: [1]
    WARNING 4: Same path as parent. Nothing to do...!
    Check target:[21;1] with declared distance [99999] and visited status: [0]
    Check target:[19;1] with declared distance [99999] and visited status: [0]
    [verzulli@XPSGarr DAY_12]$ perl solve_day12.pl | head -n 30
    Coordinates are [<ROW/Y>;<COLUMN/X>]
    Map size: [41]x[161]
    From S: [20;0] To E: [20;138]
    Working with: [20;0]/1 - starting distance: [0] - visited: [0] - CURRENT DISTANCE: [0]
    WARNING: this has been requeued!
    Check target:[20;1] with declared distance [99999] and visited status: [0]
    Check target:[21;0] with declared distance [99999] and visited status: [0]
    Check target:[19;0] with declared distance [99999] and visited status: [0]
    Working with: [20;0]/1 - final distance: [0] - visited: [0] - CURRENT DISTANCE: [0]
    Currently visited: [20;0-D:0] 
    Queue: [20;1-D:99999][21;0-D:99999][19;0-D:99999]
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    [...]
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Working with: [14;140]/24 - starting distance: [99999] - visited: [0] - CURRENT DISTANCE: [448]
    Check target:[14;141] with declared distance [405] and visited status: [1]
    Current Node:[14;140]/distance:[99999] - Target Node:[14;141]/distance:[405] - Our path distance value: [448]
    WARNING 2: NOT Updating backward. Too high level-differenct...!
    Check target:[14;139] with declared distance [401] and visited status: [1]
    Current Node:[14;140]/distance:[99999] - Target Node:[14;139]/distance:[401] - Our path distance value: [448]
    WARNING 2: NOT Updating backward. Too high level-differenct...!
    Check target:[15;140] with declared distance [447] and visited status: [1]
    Current Node:[14;140]/distance:[99999] - Target Node:[15;140]/distance:[447] - Our path distance value: [448]
    WARNING 4: Same path as parent. Nothing to do...!
    Check target:[13;140] with declared distance [403] and visited status: [1]
    Current Node:[14;140]/distance:[448] - Target Node:[13;140]/distance:[403] - Our path distance value: [448]
    WARNING 2: NOT Updating backward. Too high level-differenct...!
    Working with: [14;140]/24 - final distance: [448] - visited: [0] - CURRENT DISTANCE: [448]
    Currently visited: [0;0-D:20] [0;1-D:21] [0;2-D:22] [0;3-D:23] [0;4-D:24] [0;5-D:25] [0;6-D:26] [0;7-D:27] [0;8-D:28] [0;9-D:29] 
    Queue: 
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Distance from S@[20;0] to E:[20;138] is [440]
    [verzulli@XPSGarr DAY_12]$ 

Results for P1 - with library
=============================

    [verzulli@XPSGarr DAY_12]$ perl solve_day12_with_library.pl 
    Coordinates are [<ROW/Y>;<COLUMN/X>]
    Map size: [41]x[161]
    From S: [20;0] To E: [20;138]
    Shortest path len: [440]/[440]
    [verzulli@XPSGarr DAY_12]$ 

Results for P2
==============

    [verzulli@XPSGarr DAY_12]$ perl solve_day12_P2.pl 
    Coordinates are [<ROW/Y>;<COLUMN/X>]
    Map size: [41]x[161]
    From S: [20;0] To E: [20;138]
    Working with [0;0]: distance: [450]
    Working with [1;0]: distance: [449]
    Working with [2;0]: distance: [448]
    Working with [3;0]: distance: [447]
    Working with [4;0]: distance: [446]
    Working with [5;0]: distance: [445]
    Working with [6;0]: distance: [444]
    Working with [7;0]: distance: [443]
    Working with [8;0]: distance: [442]
    Working with [9;0]: distance: [441]
    Working with [10;0]: distance: [440]
    Working with [11;0]: distance: [439]
    Working with [12;0]: distance: [440]
    Working with [13;0]: distance: [441]
    Working with [14;0]: distance: [442]
    Working with [15;0]: distance: [443]
    Working with [16;0]: distance: [444]
    Working with [17;0]: distance: [443]
    Working with [18;0]: distance: [442]
    Working with [19;0]: distance: [441]
    Working with [20;0]: distance: [440]
    Working with [21;0]: distance: [441]
    Working with [22;0]: distance: [442]
    Working with [23;0]: distance: [443]
    Working with [24;0]: distance: [444]
    Working with [25;0]: distance: [445]
    Working with [26;0]: distance: [446]
    Working with [27;0]: distance: [447]
    Working with [28;0]: distance: [446]
    Working with [29;0]: distance: [447]
    Working with [30;0]: distance: [448]
    Working with [31;0]: distance: [449]
    Working with [32;0]: distance: [450]
    Working with [33;0]: distance: [451]
    Working with [34;0]: distance: [452]
    Working with [35;0]: distance: [453]
    Working with [36;0]: distance: [454]
    Working with [37;0]: distance: [455]
    Working with [38;0]: distance: [456]
    Working with [39;0]: distance: [457]
    Working with [40;0]: distance: [458]
    Minimun LEN: [439]
    [verzulli@XPSGarr DAY_12]$ 
