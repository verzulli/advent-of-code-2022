#!/usr/bin/perl

use strict;
use Data::Dumper;
use Term::Cap;

$|=1;
$Data::Dumper::Maxdepth = 2;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------

sub ProcessQueue();
sub DumpMap();
sub GetKey($$);
sub SuperDebug();
sub DumpVisited();
sub isEnqueued($$$$);
sub Reset();

my $map; # array of array_refs, for global MAP;
my $working_queue; # array of HASH refs

my @path; # array of Y|X, Y|X, Y|X.... of current path

# Let's open the file for reading...
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Let's load the data and populate the in-memory data structure (%monkeys)...
while (my $line = <FH>) {

  # remove ending newline
  chomp($line);

  #split the line and populate the array
  my @row_list = split(//, $line); 
  push(@{$map}, \@row_list);

}

my $infinity = 99999;
my $num_map_rows = scalar @{$map};
my $num_map_cols = scalar @{$map->[0]};

# Start position - "S"
my %S = (
  'x' => -1,
  'y' => -1,
);

my %E = (
  'x' => -1,
  'y' => -1,
);

my $end_y = -1;

for (my $r = 0; $r < $num_map_rows; $r++) {
  my $cols = $map->[$r];
  for (my $c = 0; $c < $num_map_cols; $c++) {
    my $cur_elevation = $map->[$r][$c];
    if ($cur_elevation eq 'S') {
      $S{'X'} = $c;
      $S{'Y'} = $r;
      $map->[$r][$c] = {
        'elevation' => ord('a')-96,
        'visited' => 0,
        'distance' => 0,
        'row' => $r,
        'column' => $c,
        'parent' => {}
      };
    } elsif ($cur_elevation eq 'E') {
      $E{'X'} = $c;
      $E{'Y'} = $r;
      $map->[$r][$c] = {
        'elevation' => ord('z')-96,
        'visited' => 0,
        'distance' => $infinity,
        'row' => $r,
        'column' => $c,
        'parent' => {}
      };
    } else {
      $map->[$r][$c] = {
        'elevation' => ord($map->[$r][$c])-96,
        'visited' => 0,
        'distance' => $infinity,
        'row' => $r,
        'column' => $c,
        'parent' => {}
      };
    }
  }
}

printf("Coordinates are [<ROW/Y>;<COLUMN/X>]\n");
printf("Map size: [%d]x[%d]\n", $num_map_rows, $num_map_cols);
printf("From S: [%d;%d] To E: [%d;%d]\n", $S{'Y'}, $S{'X'}, $E{'Y'}, $E{'X'});

# debug
# DumpMap();

my $tree_depth = 0;
my $global_iteration_counter = 0;
my %findings;
my $t = Term::Cap->Tgetent;
my %max = (
  'depth' => 0,
  'x' => -1,
  'y' => -1
 );

# Let's loop over all the "a" cells
my $min_len = $infinity;

for (my $r = 0; $r < $num_map_rows; $r++) {
  # printf("Row [%3d]: ", $r);
  my $cols = $map->[$r];

  #Let's "reset" the whole stuff
  Reset();

  for (my $c = 0; $c < $num_map_cols; $c++) {
    if ($map->[$r][$c]{'elevation'} == ord('a')-96) {

      # Start processing the START node, with distance 0 and empty parent (hash ref)
      printf("Working with [%d;%d]: ",$r, $c );
      push (@{$working_queue}, [ $r, $c, 0, {} ] );
      ProcessQueue();
      my $distance = $map->[$E{'Y'}][$E{'X'}]{'distance'};
      $min_len = $distance if ($distance < $min_len);
      print "distance: [".$distance."]\n";
      last;
    }
  };
}
print "Minimun LEN: [".$min_len."]\n";

# print "\n\nFindings report:\n";
# # foreach my $f (sort { {$findings{$a} <=> $findings{$b} }} keys %findings ) {
# foreach my $f (sort keys %findings ) {
#   printf("Len: [%d] - [%d] times\n", $f, $findings{$f});
# }

exit;

sub Reset() {
  for (my $r = 0; $r < $num_map_rows; $r++) {
    for (my $c = 0; $c < $num_map_cols; $c++) {
      $map->[$r][$c]{'visited'}=0;
      $map->[$r][$c]{'distance'}=0;
      $map->[$r][$c]{'parent'} = {};
    }
  }
};

sub DumpMap() {
  for (my $r = 0; $r < $num_map_rows; $r++) {
    # printf("Row [%3d]: ", $r);
    my $cols = $map->[$r];
    for (my $c = 0; $c < $num_map_cols; $c++) {
      my $cur_elevation = $map->[$r][$c]{'elevation'};
      # printf(" %2d",$cur_elevation);
    };
    # print "\n";
  }
}

# Receive COL and ROW, ...and distance of caller
sub ProcessQueue() {

  while (my $item = shift @{$working_queue}) {
    my ($r, $c, $distance, $prev_node_ref) = ($item->[0], $item->[1], $item->[2], $item->[3]);

    # printf("S: [%d]\n",$map->[$E{'Y'}][$E{'X'}]{'distance'});
    # hash ref to THIS node...
    my $this_node_ref = $map->[$r][$c];

    $global_iteration_counter++;
    $tree_depth++; 

    # debug
    # print "=" x $tree_depth;
    # printf("Working with: [%d;%d]/%d - starting distance: [%d] - visited: [%d] - CURRENT DISTANCE: [%d]\n",
    #   $r, 
    #   $c, 
    #   $this_node_ref->{'elevation'},
    #   $this_node_ref->{'distance'},
    #   $this_node_ref->{'visited'},
    #   $distance
    # );
    # if ($this_node_ref->{'distance'} != $infinity) {
    #   printf("WARNING: this has been requeued!\n");
    # }
    # printf("Current distance from S@[%d;%d] to E:[%d;%d] is [%d]\n", $S{'Y'}, $S{'X'}, $E{'Y'}, $E{'X'}, $this_node_ref->{'distance'} );

    # print $t->Tgoto("cm", 0, 0); # 0-based
    # SuperDebug();

    # Let's create the array of targets to visit
    my @next_cells=();
    if ($c < $num_map_cols - 1) {
      if ($this_node_ref->{'elevation'}+1 >= $map->[$r][$c+1]{'elevation'}) {
        # Add RIGHT node, as candidate
        push (@next_cells, [$r, $c+1]);
      }
    }
    if ($c > 0) {
      if ($this_node_ref->{'elevation'}+1 >= $map->[$r][$c-1]{'elevation'}) {
        # Add LEFT node, as candidate
        push (@next_cells, [$r, $c-1]);
      }
    }
    if ($r < $num_map_rows - 1) {
      if ($this_node_ref->{'elevation'}+1 >= $map->[$r+1][$c]{'elevation'}) {
        # Add DOWN node, as candidate
        push (@next_cells, [$r+1, $c]);
      }
    }
    if ($r > 0) {
      if ($this_node_ref->{'elevation'}+1 >= $map->[$r-1][$c]{'elevation'}) {
        # Add UP node, ad candidate
        push (@next_cells, [$r-1, $c]);
      }
    }

    # Let's process targets...
    foreach my $cell (@next_cells) {

      my $target_ref = $map->[$cell->[0]][$cell->[1]];

      #   printf("Check target:[%d;%d] with declared distance [%d] and visited status: [%d]\n",
      #     $cell->[0],
      #     $cell->[1],
      #     $target_ref->{'distance'},
      #     $target_ref->{'visited'}
      #   );

      # If it has been visited... 
      if ( $target_ref->{'visited'} == 1) {

        # Visited! So, if we're here, it means that two "paths" have $target_ref node in common. 
        # This imply two things:
        # 1. if the other path is "shorter", than we have to update our "distance", backward
        #    along the path that took us here. Hence, we have to RE-queue our parent, aka: this_node_ref->parent
        # 2. if the other path is "longer", than we are OK with our "distance" but the other
        #    path (aka: target_ref->parent) need to be updated, as currently it can be reached faster via this
        #    path
    
        # debug
        # printf("Current Node:[%d;%d]/distance:[%d] - Target Node:[%d;%d]/distance:[%d] - Our path distance value: [%d]\n",
        #   $r,
        #   $c,
        #   $this_node_ref->{'distance'},
        #   $cell->[0],
        #   $cell->[1],
        #   $target_ref->{'distance'},
        #   $distance
        # );

        # ...let's see if TARGET is "closer" than distance
        if ($target_ref->{'distance'} + 1 < $distance ) {
          # this target (target_ref) is visited, and trough it, we have a shorter path.
          # We're in case 1. above

          # Anyway.... we've to check if backwardly we have only "1 step" (...and no more)
          if (abs($target_ref->{'elevation'} - $this_node_ref->{'elevation'}) < 1) {
            # debug
            # printf("WARNING 1: Updating backward...!\n");

            # so:
            # - 1: let's set the shorter distance of main node, via this target
            $this_node_ref->{'distance'} = $target_ref->{'distance'} +1;

            # - 2: let's reschedule check for main node's "parent" (as it could be closer, now)
            my $mn_parent_ref = $this_node_ref->{'parent'};
                      
            my $found = isEnqueued($mn_parent_ref->{'row'}, $mn_parent_ref->{'column'}, $target_ref->{'distance'} + 1, $this_node_ref);
            if (! $found) {
              unshift (@{$working_queue}, [$mn_parent_ref->{'row'}, $mn_parent_ref->{'column'}, $target_ref->{'distance'} + 1, $this_node_ref]);
            }
          } else {
            # printf("WARNING 2: NOT Updating backward. Too high level-differenct...!\n");
          }

        } elsif ($target_ref->{'distance'} + 1 > $distance ) {

          # We're in case 2. above

          # debug
          # printf("WARNING 3: Updating forward...!\n");

          # let's set THIS NODE's distance
          $this_node_ref->{'distance'} = $distance;

          # let's update the TARGET and REQUEUE it
          $target_ref->{'distance'} = $distance + 1;

        } else {
          # debug
          # printf("WARNING 4: Same path as parent. Nothing to do...!\n");
          $this_node_ref->{'distance'} = $distance;
        };

      } else {
        # target has NOT been visited...

        my $found = isEnqueued($cell->[0], $cell->[1], $distance, $this_node_ref);

        if (! $found) {
          # it's not already in the queue. Let's add it... with back-reference to "this node hashref"
          push (@{$working_queue}, [$cell->[0], $cell->[1], $distance + 1, $this_node_ref]);

          # Let's track parent-child relation, in the map
          $target_ref->{'parent'} = $this_node_ref;
        } else {
          # printf("WARNING: NOT Enqueuing, as already queued...!\n");
        }
        $this_node_ref->{'distance'} = $distance;
      }
    }

    # printf("Working with: [%d;%d]/%d - final distance: [%d] - visited: [%d] - CURRENT DISTANCE: [%d]\n",
    #   $r, 
    #   $c, 
    #   $this_node_ref->{'elevation'},
    #   $this_node_ref->{'distance'},
    #   $this_node_ref->{'visited'},
    #   $distance
    # );

    # Just finished working on this_node_ref. Set 'visited' to true
    $this_node_ref->{'visited'} = 1;

    # debug
    # DumpVisited();

    # debug
    # print "Queue: ";
    # foreach my $idx (0..$#{$working_queue}) {
    #   my $q_item_ref = $working_queue->[$idx];
    #   printf("[%d;%d-D:%d]",$q_item_ref->[0], $q_item_ref->[1],$map->[$q_item_ref->[0]][$q_item_ref->[1]]{'distance'} );
    #   if ($idx > 20) {
    #     print "...[etc.]"; last;
    #   }
    # }
    # print "\n";

    # if (($r == $E{'Y'}) && ($c == $E{'X'})) {
    #   printf("Find Destination, at [%d;%d], at distance [%d], at iteration: [%d]\n", $r, $c, $map->[$r][$c]{'distance'}, $global_iteration_counter);
    # }
    # print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n";

  }
}

sub GetKey($$) {
  my ($r, $c) = @_;

  my $key = sprintf("%d;%d",$r, $c);
  return $key;
}

sub SuperDebug() {
  # print "-" x 20; print "\n";

  # Print header
  print "     |";
  foreach my $c (0..$num_map_cols-1) {
    printf("%4d", $c);
  }
  print "\n";

  # Print rows
  foreach my $r (0..$num_map_rows-1)  {
    printf("%4d |", $r);
    foreach my $c (0..$num_map_cols-1) {
      if ($map->[$r][$c]{'distance'} == $infinity) {
        printf("  - ");
      } else {
        printf("%4d", $map->[$r][$c]{'distance'});
      }
    }
    print "\n";
  }
  print "\n";
}

sub DumpVisited() {
  my $idx = 1;
  print "Currently visited: ";
  foreach my $r (0..$num_map_rows-1)  {
    foreach my $c (0..$num_map_cols-1) {
      if ($map->[$r][$c]{'visited'}) {
        printf("[%d;%d-D:%d] ", $r, $c, $map->[$r][$c]{'distance'});
        $idx++;
      }
      last if($idx>10);
    }
    last if($idx>10);
  }
  print "\n";
};

sub isEnqueued($$$$) {
  my ($r, $c, $distance, $parent_ref) = @_;

  # let's see if it has already been queued from some other path...
  my $found = 0;
  foreach my $idx (0..$#{$working_queue}) {
    my $q_item_ref = $working_queue->[$idx];
    if (($q_item_ref->[0]==$r) && ($q_item_ref->[1]==$c)) {
      # Yes! It's already in the queue...
      
      # Let's see if this path is "better"
      if ($q_item_ref->[2] > $distance + 1) {
        # this way, it's shorter... let's update the reference
        $q_item_ref->[2]=$distance + 1;
        $q_item_ref->[3]=$parent_ref;
      }
      $found = 1;
    }
  }

  return $found
}
