TL/DR;
======
You have a sequence of NOOP (requiring 1 clock-cycle) and ADDX (requiring two-clock-cycles and really performing the ADD at the **END** of the second cycle).

Start with `X=1`, follow its value and print it at clock cycles 20, 60, 100, 140, 180, 120.

Sum the values (multiplied by related clock cycle) and get the result.

As for P2, basically you have a framebuffer associated to a CRT and based on X register (pointer in the framebuffer) and cycle reference (CRT drawer), you turn on pixels (if they are close enough). So, draw che CRT and get the 8 resulting chars

Original text
=============

    [...]
    Start by figuring out the signal being sent by the CPU. The CPU has a single register, X, which starts with the value 1. It supports only two instructions:

    addx V takes two cycles to complete. After two cycles, the X register is increased by the value V. (V can be negative.)
    noop takes one cycle to complete. It has no other effect.

    Maybe you can learn something by looking at the value of the X register throughout execution. For now, consider the signal strength (the cycle number multiplied by the value of the X register) during the 20th cycle and every 40 cycles after that (that is, during the 20th, 60th, 100th, 140th, 180th, and 220th cycles).

    Find the signal strength during the 20th, 60th, 100th, 140th, 180th, and 220th cycles. What is the sum of these six signal strengths?
    [...]

As for P2:

    Like the CPU, the CRT is tied closely to the clock circuit: the CRT draws a single pixel during each cycle. Representing each pixel of the screen as a #, here are the cycles during which the first and last pixel in each row are drawn:
    [...]
    So, by carefully timing the CPU instructions and the CRT drawing operations, you should be able to determine whether the sprite is visible the instant each pixel is drawn. If the sprite is positioned such that one of its three pixels is the pixel currently being drawn, the screen produces a lit pixel (#); otherwise, the screen leaves the pixel dark (.). 
    [...]
    Render the image given by your program. What eight capital letters appear on your CRT?


My notes
========
I'm going to implement the solution in [PERL](https://www.perl.org).

* My input data are in the "input.txt"
* My implemented solutions are in `solve_day10.pl`

**I'm pretty proud about the *additional code* that was required by P2 implementation.**

Solving approach
----------------
It has been a bit of mess to properly keep track of array position (where I hold the values) and the point-in-time where `X` value change.

But aside for such a "syncronization" problem... P1 was really simple:

* create an array with values associated to each clock cycle;
* process the array to retrieve what we need

As for P2:

* just rewalk along the cycle array, keeping track of...
* ...line to draw (when % 40 is zero, just print and go to newline);
* deltas between CRT position and X register.

Very easy (and elegant!), BTW!

Implementation
==============

Please:
* refer to [`solve_day10.pl`](./solve_day10.pl)

Results (for both P1 and P2)
============================

    [verzulli@XPSGarr DAY_10]$ perl solve_day10.pl 
    STRENGTH: [20] x [21] = [420] - TOTAL: [420]
    STRENGTH: [60] x [23] = [1380] - TOTAL: [1800]
    STRENGTH: [100] x [25] = [2500] - TOTAL: [4300]
    STRENGTH: [140] x [14] = [1960] - TOTAL: [6260]
    STRENGTH: [180] x [5] = [900] - TOTAL: [7160]
    STRENGTH: [220] x [21] = [4620] - TOTAL: [11780]
    Dumping CRT...
    LINE: ###..####.#..#.#....###...##..#..#..##..
    LINE: #..#....#.#..#.#....#..#.#..#.#..#.#..#.
    LINE: #..#...#..#..#.#....###..#..#.#..#.#..#.
    LINE: ###...#...#..#.#....#..#.####.#..#.####.
    LINE: #....#....#..#.#....#..#.#..#.#..#.#..#.
    LINE: #....####..##..####.###..#..#..##..#..#.
    [verzulli@XPSGarr DAY_10]$ 

So the chars are **PZULBAUA**