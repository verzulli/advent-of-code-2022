#!/usr/bin/perl

use strict;

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------


my @cicles = ( 0 );

# Let's open the file for reading...
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Load program in memory...
while (my $line = <FH>) {

  # remove ending newline
  chomp($line);

  # Do we have a noop?
  if ($line =~ /^noop$/) {
    push(@cicles, 0);
  } elsif ($line =~ /^addx\s(\-{0,1}\d+)$/) {
    my $v = $1;
    push(@cicles, 0);
    push(@cicles, $v);
  }
}

my $x = 1;
my $tot_strength = 0;
for my $idx (1..$#cicles) {
  $x = $x + $cicles[$idx-1];
  # printf("cicle: [%3d] => %d\n",$idx,$x);

  if ($idx >= 20 and (($idx - 20 ) % 40) == 0) {
    $tot_strength = $tot_strength + ($idx * $x);
    printf("STRENGTH: [%d] x [%d] = [%d] - TOTAL: [%d]\n", $idx, $x, $idx * $x, $tot_strength);
  }

}


print "Dumping CRT...\n";

# debug
# printf("cycle CRT_POS SPRITE_POS DELTA\n");

$x = 1;
my $line = '';

# RE-loop over the whole cycles
for my $idx (1..$#cicles) {

  # calculate CRT position and SPRITE/CRT DELTA
  my $crt_pos = ($idx - 1) % 40;
  my $delta = abs($crt_pos - $x);

  # debug
  # printf("%5d %7d  %9d %5d\n",$idx, $crt_pos, $x, $delta);

  # Let's see if sprite position overlap with CRT position
  if ($delta <= 1) {
    # if yes, print a "#""
    $line = $line . '#';
  } else {
    # if no, print "."
    $line = $line . '.';
  }

  # if we're at the end of a line, just write the line and reset it's content
  if (($idx % 40 ) == 0) {
    print "LINE: " . $line . "\n";
    $line = '';
  }

  $x = $x + $cicles[$idx];

}

exit;

