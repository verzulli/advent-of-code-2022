#!/usr/bin/perl

# ------------------------------------------------------------------------------
# Copyright 2022, Damiano Verzulli - damiano.verzulli@garrlab.it
#
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU Affero General Public License as published by the 
# Free Software Foundation, either version 3 of the License, or (at your 
# option) any later version.
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
# for more details.
# You should have received a copy of the GNU Affero General Public License 
# along with this program. If not, see <https://www.gnu.org/licenses/>. 
# ------------------------------------------------------------------------------
  
# Let's open the file for reading
# ###
# ### WARNING! Input file **NEED** to have **TWO** blank lines at the end...
# ### (...otherwise _LOTS_ of complexities arise with the code...)
# ###
open(FH, "./input.txt") || die ("Unable to open input.txt: $!");

# Let's initialize the temporary SUM
my $tSum = 0;

# ...and the max sum variable
my $maxSum = 0;

# AoC-Part2: let's keep track of block-totals, for top-three-list
my @topList = ();

# Let's read the file, line by line
while (my $int = <FH>) {

  # let's get rid of the terminating newline
  chomp($int);

  # Let's check if we met a newline (taking care of possible spaces..)
  if ($int =~ /^\s*$/) {

    # AoC-Part2: push total to the list
    push(@topList, $tSum);

    # let's check if we have a new MAX sum
    if ($tSum > $maxSum) {
      $maxSum = $tSum;
      # we are kind, so let's tell the user we found a new max
      print "New max found: [" . $maxSum . "]\n";
    }
    # reset the temporary sum
    $tSum = 0;
  } elsif ($int =~ /^\d+$/) {
    # we have numbers... so we can proceed adding them to temp sum
    # (we thanks perl for string=>int casting :-)
    $tSum = $tSum + $int;
  } else {
    # we got something strange so... let's throw away the line
    print "Ignoring bad line: [" . $int . "]\n"
  }
}

# AoC-Part2: descend-sort the total list + pick the top-three and sum them
my @topListSorted = sort { $b <=> $a } @topList;
my @topThree = @topListSorted[0,1,2];
print "TopList: [" . join(';', @topThree) . "]\n";
my $tScore = $topThree[0] + $topThree[1] + $topThree[2];
print "Total: ". $tScore. "\n"