TL/DR;
======
Given a variable number of "set of integers", where each set is separated from the other by a blank line (one integer per line), find the sum of the integer related to the set whose sum is the highest


Original text
=============

    [...]
    The Elves take turns writing down the number of Calories contained by the various meals, snacks, rations, etc. that they've brought with them, one item per line. Each Elf separates their own inventory from the previous Elf's inventory (if any) by a blank line.

    For example, suppose the Elves finish writing their items' Calories and end up with the following list:

        1000
        2000
        3000

        4000

        5000
        6000

        7000
        8000
        9000

        10000

    This list represents the Calories of the food carried by five Elves:

    The first Elf is carrying food with 1000, 2000, and 3000 Calories, a total of 6000 Calories.
    The second Elf is carrying one food item with 4000 Calories.
    The third Elf is carrying food with 5000 and 6000 Calories, a total of 11000 Calories.
    The fourth Elf is carrying food with 7000, 8000, and 9000 Calories, a total of 24000 Calories.
    The fifth Elf is carrying one food item with 10000 Calories.

    In case the Elves get hungry and need extra snacks, they need to know which Elf to ask: they'd like to know how many Calories are being carried by the Elf carrying the most Calories. In the example above, this is 24000 (carried by the fourth Elf).

    Find the Elf carrying the most Calories. How many total Calories is that Elf carrying?

My notes
========
I'm going to implement the solution in [PERL](https://www.perl.org), as it's perfect to read input file and work properly on it.

* My input data are in the "input.txt"
* My implemented solution is "solve_day1.pl"

Solving approach
----------------
I'll read the file, line by line, adding the integer to the temporary variable `$tSum`, which will hold the temporary group sum.

When a blank like will be encountered, if the `$tSum` is higher then `$maxSum` (holding the previous max), I'll update `$maxSum`.

A minimal sanitize-check of inputs, are provided as well

As for Part-two, group sum will be stored in an array. At the end, the array is ordered and the first three elements are summarized.

Implementation
==============

Please refer to [`solve_day1.pl`](./solve_day1.pl)

Results
=======

    [verzulli@XPSGarr DAY_01]$ perl solve_day1.pl 
    New max found: [48048]
    New max found: [55328]
    New max found: [63195]
    New max found: [67468]
    New max found: [69921]
    New max found: [72718]
    [verzulli@XPSGarr DAY_01]$ 

Results - part TWO
==================

    [verzulli@XPSGarr DAY_01]$ perl solve_day1.pl 
    New max found: [48048]
    New max found: [55328]
    New max found: [63195]
    New max found: [67468]
    New max found: [69921]
    New max found: [72718]
    TopList: [72718;70450;69921]
    Total: 213089
    [verzulli@XPSGarr DAY_01]$ 
